<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Model\AboutUs;
use App\Model\WhoWeAre;
use App\Model\HomePage;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Developer',
            'email' => 'developer@mkoratec.com',
            'password' => Hash::make('123456'),
        ]);

        (new AboutUs())->save();
        (new WhoWeAre())->save();
        (new HomePage())->save();
    }
}
