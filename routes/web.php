<?php

Route::namespace('Admin')->group(function(){
    Route::namespace('Auth')->group(function(){
        Route::get('/login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
        Route::post('/login', ['uses' => 'LoginController@login', 'as' => 'postLogin']);
        Route::post('/logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });
    
    Route::group(['middleware' => 'auth','prefix' => 'admin'], function(){
        Route::get('/dashboard',['uses' => 'DashboardController@index', 'as' => 'dashboard']);

        Route::group(['middleware' => ['permission:view-user|user-modification|add-new-user']], function () {
            Route::resource('/users','UserController');
        });
        Route::group(['middleware' => ['permission:view-role|role-modification|add-new-role']], function () {
            Route::resource('/roles','UserRoleController');
        });
        Route::resource('news','NewsController');


        Route::group(['middleware' => ['permission:view-project-category|project-category-modification|add-new-project-category']], function () {
            Route::resource('projects/categories','ProjectCategoryController',['as' => 'projects']);
        });
        Route::group(['middleware' => ['permission:view-project|project-modification|add-new-project']], function () {
            Route::resource('projects','ProjectController');
        });

        Route::group(['middleware' => ['permission:view-career|career-modification|add-new-career']], function () {
            Route::resource('careers','CareerController');
        });

        Route::group(['middleware' => ['permission:view-page-image|page-image-modification']], function () {
            Route::get('settings/page-image',['uses' => 'PageImageController@index','as' => 'settings.page-image.index']);
            Route::post('settings/page-image',['uses' => 'PageImageController@update','as' => 'settings.page-image.update']);
        });

        Route::group(['middleware' => ['permission:view-seo|seo-modification']], function () {
            Route::get('settings/seo',['uses' => 'SeoController@index','as' => 'settings.seo.index']);
            Route::post('settings/seo',['uses' => 'SeoController@update','as' => 'settings.seo.update']);
        });
        Route::group(['middleware' => ['permission:view-social-media|social-media-modification']], function () {
            Route::get('settings/social-media',['uses' => 'SocialMediaController@index','as' => 'settings.social-media.index']);
            Route::post('settings/social-media',['uses' => 'SocialMediaController@update','as' => 'settings.social-media.update']);
        });

        Route::prefix('who-are-we')->group(function(){
            Route::group(['middleware' => ['permission:company-profile']], function () {
                Route::get('/',['uses' => 'WhoAreWeController@index','as' => 'whoAreWe.index']);
                Route::patch('updateProfile',['uses' => 'WhoAreWeController@updateProfile','as' => 'whoAreWe.updateProfile']);
            });

            Route::group(['middleware' => ['permission:management-team']], function () {
                Route::post('save-management-team','WhoAreWeController@saveManagementTeam');
                Route::post('update-management-team/{id}','WhoAreWeController@updateManagementTeam');
                Route::get('get-management-team','WhoAreWeController@getManagementTeam');
                Route::delete('remove-management-team/{id}','WhoAreWeController@removeManagementTeam');
            });

            Route::group(['middleware' => ['permission:chairman-message']], function () {
                Route::patch('update-message-from-ceo',['uses' => 'WhoAreWeController@update_message','as' => 'whoAreWe.updateMessage']);
            });
        });

        Route::group(['middleware' => ['permission:banner-slider|our-story|video']], function () {
            Route::get('home-page',['uses' => 'HomePageController@index','as' => 'homePage.index']);
            Route::get('home-page/get-slider',['uses' => 'HomePageController@getSlider','as' => 'homePage.getSlider']);
            Route::post('home-page/save-sliders',[ 'uses' => 'HomePageController@saveSlider' ,'as' => 'homePage.saveSlider']);
            Route::post('home-page/remove-file','HomePageController@removeFile');
            Route::post('home-page/discover-story',['uses' => 'HomePageController@saveDiscoverStory','as' => 'homePage.discoverStory']);
            Route::post('home-page/video',['uses' => 'HomePageController@saveVideoKey','as' => 'homePage.saveVideoKey']);
        });

        Route::group(['middleware' => ['permission:contact-us-page']], function () {
            Route::get('contact-us',['uses' => 'ContactUsController@index','as' => 'contactUs.index']);
            Route::patch('contact-us',['uses' => 'ContactUsController@update', 'as' => 'contactUs.update']);
        });

        Route::group(['middleware' => ['permission:about-us-page']], function () {
            Route::prefix('about-us')->group(function(){
                Route::get('/',['uses' => 'AboutUsController@index','as' => 'aboutUs.index']);
                Route::patch('/',['uses' => 'AboutUsController@update','as' => 'aboutUs.update']);
                Route::patch('update-header-image',['uses' => 'AboutUsController@updateHeaderImage','as' => 'aboutUs.updateHeaderImage']);
                Route::patch('update-company-description',['uses' => 'AboutUsController@updateCompanyDescription','as' => 'aboutUs.updateCompanyDescription']);
                Route::patch('update-company-profile',['uses' => 'AboutUsController@updateCompanyProfile','as' => 'aboutUs.updateCompanyProfile']);
            });
        });

        Route::resource('/users/profile','EditProfileController',['as' => 'users']);

        //ajax upload image
        Route::post('upload-image','ImageController@uploadImage');
        Route::post('upload-temp-multiple',['as' => 'dropzonesSaveMultiple', 'uses' => 'ImageController@uploadTempMultiple']);
        Route::post('/save-temp-image', ['as' => 'dropzoneSaveTempImage', 'uses' => 'ImageController@dropzoneSaveTempImage']);
    });  
});

Route::group(['namespace' => 'Client','as' =>'client.'],function(){
    Route::get('/',['uses' => 'FrontendController@index','as' => 'homepage']);
    Route::get('/who-we-are',['uses' => 'FrontendController@whoWeAre','as' => 'who-we-are']);
    Route::get('/about-us',['uses' => 'FrontendController@aboutUs','as' => 'about-us']);
    Route::get('/our-projects',['uses' => 'FrontendController@ourProjects','as' => 'our-projects']);
    Route::get('/our-projects/{category}',['uses' => 'FrontendController@projectCategory','as' => 'project-category']);
    Route::get('/contact',['uses' => 'FrontendController@contact','as' => 'contact']);
    Route::get('/career-opportunities',['uses' => 'FrontendController@career','as' => 'career-opportunities']);
    Route::get('/career-opportunities/{id}',['uses' => 'FrontendController@careerDetail','as' => 'career-detail']);
    Route::get('/news/{id}',['uses' => 'FrontendController@newsDetail', 'as' => 'news-detail']);
    Route::get('/news',['uses' => 'FrontendController@news', 'as' => 'news']);
});
