<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use View;
use App;
use App\Model\ProjectCategory;
use App\Model\ContactUs;
use App\Model\Setting;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191); 
        view()->composer('*', function ($view) {
            $view->with('projectCategories', ProjectCategory::whereHas('projects')->with('projects')->get()->keyBy('id'));
            $view->with('contactUs', ContactUs::first());
            $view->with('settings', Setting::all()->keyBy('key'));
        });


    }
}
