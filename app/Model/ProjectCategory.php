<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
    protected $fillable = [
        'name'
    ];

    public function projects(){
        return $this->hasMany(Project::class,'category_id')->orderBy('id','DESC');
    }
}
