<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class WhoWeAre extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'bigbiz_profile','chairman_name','message'
    ];
}
