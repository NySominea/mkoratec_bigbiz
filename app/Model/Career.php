<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $fillable = [
        'title','level','qualification','number','experience','language','function',
        'salary','description','started_date','expired_date','location','short_description'
    ];

}
