<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class ContactUs extends Model implements HasMedia
{
    use HasMediaTrait;
    
    protected $fillable = [
        'email','tel','fax','address','map'
    ];
}
