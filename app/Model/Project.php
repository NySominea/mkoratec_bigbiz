<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;

class Project extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'name','description','client','location','year','value','architect','category_id'
    ];

    public function category(){
        return $this->belongsTo(ProjectCategory::class,'category_id');
    }
}
