<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Project;
use App\Model\ProjectCategory;
use App\Model\Career;
use App\Model\AboutUs;
use App\Model\WhoWeAre;
use App\Model\ManagementTeam;
use App\Model\News;
use App\Model\HomePage;

class FrontendController extends Controller
{
    public function index(){
        $sliders = HomePage::first();
        $news = News::orderBy('id','desc')->take(4)->get();
        $images = HomePage::first()->getMedia('slider');
        $home_page = HomePage::first();
        return view('client.homepage.index',compact('news','images','home_page','sliders'));
    }

    public function whoWeAre(){
        $about_us = AboutUs::first();
        $who_are_we = WhoWeAre::first();
        $management_teams = ManagementTeam::all();
        return view('client.who-we-are.index',compact('about_us','who_are_we','management_teams'));
    }

    public function aboutUs(){
        $about_us = AboutUs::first();
        return view('client.about-us.index',compact('about_us'));
    }

    public function ourProjects(){
        $projects = Project::orderBy('id','DESC')->get();
        return view('client.our-project.index',compact('projects'));
    }

    public function projectCategory($id){
        return view('client.our-project.sub',compact('id'));
    }

    public function contact(){
        $about_us = AboutUs::first();
        return view('client.contact.index',compact('about_us'));
    }

    public function career(){
        $careers = Career::where('started_date','<=', date('Y-m-d'))
                        ->where('expired_date','>=', date('Y-m-d'))
                        ->orderBy('expired_date','ASC')
                        ->get();
        return view('client.career.index',compact('careers'));
    }
    public function careerDetail($id){
        $career = Career::findOrFail($id);
        $newCareers = Career::where('started_date','<=', date('Y-m-d'))
                            ->where('expired_date','>=', date('Y-m-d'))
                            ->where('id','!=',$id)
                            ->orderBy('expired_date','ASC')
                            ->get();
        return view('client.career.detail',compact('career','newCareers'));
    }

    public function news(){
        $news = News::orderBy('id','desc')->get();
        return view('client.news.index',compact('news'));
    }
    public function newsDetail($id){
        $news = News::findOrFail($id);
        $newsList = News::orderBy('id','DESC')->where('id','!=',$id)->get();
        return view('client.news.detail',compact('news','newsList'));
    }
}
