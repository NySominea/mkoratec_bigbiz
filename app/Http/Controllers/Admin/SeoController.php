<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Setting;

class SeoController extends Controller
{
    public function index(){
        $settings = Setting::all()->keyBy('key');
        return view('admin.seo.index',compact('settings'));
    }
    public function update(){
        
        foreach(['seo_keyword','seo_description'] as $key){
            $row = Setting::where('key',$key)->first();
            $r = ['key' => $key, 'value' => request()->{$key}];
            if($row){
                $row->update($r);
            }else{
                Setting::create($r);
            }
        }
        return redirect()->route('settings.seo.index');
    }
}
