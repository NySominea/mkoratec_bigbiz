<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Setting;

class PageImageController extends Controller
{
    public function index(){
        $settings = Setting::all()->keyBy('key');
        return view('admin.page-image.index',compact('settings'));
    }
    public function update(){
        $imaegKey = ['about_us_image','contact_us_image','news_image','project_image','career_image'];
        foreach($imaegKey as $key){
            $row = Setting::where('key',$key)->first();
            $r = [
                'key' => $key,
                'value' => isset(request()->{$key}) ? request()->{$key} : '',
            ];
            if($row){
                $row->update($r);
            }else{
                $row = Setting::create($r);
            }
            
            if(isset(request()->{$key}) && request()->{$key}){
                $row->clearMediaCollection('header-image'); 
                $row->addMedia(request()->{$key})
                            ->toMediaCollection('header-image');
                
            }
        }
        return redirect()->route('settings.page-image.index');
    }
}
