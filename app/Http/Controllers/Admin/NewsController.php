<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\News;
use DB;
use Exception; 

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::paginate(10);
        return view('admin.news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);
 
        DB::beginTransaction();

        try{
            $news = new News();
            $news->fill($request->all())->save();
            $news->addMedia(request()->get('image'))->toMediaCollection('thumbnail');
            DB::commit();

            return redirect()->route('news.index')->withSuccess('News has been added!');
        }catch(Exception $ex){
            DB::rollback();

            return redirect()->back()->withErrors([
                
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return view('admin.news.create',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required'
        ]);
 
        DB::beginTransaction();

        try{
            $news = News::findOrFail($id);
            $news->fill($request->all())->save();

            if(request()->get('image')){
                $news->clearMediaCollection('thumbnail');
                $news->addMedia(request()->get('image'))->toMediaCollection('thumbnail');
            }
            DB::commit();

            return redirect()->route('news.index')->withSuccess('News has been updated!');
        }catch(Exception $ex){
            DB::rollback();

            return redirect()->back()->withErrors([
                
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);
        $news->delete();
        return response()->json([]);
    }
}
