<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\HomePage;
use Symfony\Component\HttpFoundation\File\File;
use Spatie\MediaLibrary\Models\Media;

class HomePageController extends Controller
{
    function index(){
        $home_page = HomePage::first();
        return view('admin.home_page.index',compact('home_page'));
    }

    function saveDiscoverStory(){
        $this->validate(request(),[
            'description' => 'required'
        ]);

        $home_page = HomePage::first();
        $home_page->discover_story = request()->get('description');
        $home_page->save();
        if(request()->get('background')){
            $home_page->clearMediaCollection('background');
            $home_page->addMedia(request()->get('background'))->toMediaCollection('background');
        }
        
        return redirect()->back()->withDiscoverSuccess('Discover our story information has been updated!');
    }

    function saveVideoKey(){
        $home_page = HomePage::first();
        $home_page->video_key = request()->get('video_key');
        $home_page->save();
        
        return redirect()->back()->withVideoKey('Video youtube key has been updated!');
    }

    function saveSlider(){
        
        $images = request()->get('images');
        // dd($images);
        $homePage = HomePage::first();
        $homePage->clearMediaCollection('slider');
        foreach($images as $image){
            $homePage->addMedia(new File(public_path().$image))->preservingOriginal()->toMediaCollection('slider');
        }

        return redirect()->back()->withSliderSuccess('Slider images has been updated!');
    }

    function removeFile(){
        $file = request()->get('server_file');
        $media = Media::where('file_name',explode('/',$file)[3])->delete();
    }

    function getSlider(){
        $homePage = HomePage::first();
        $sliders = [];
        foreach($homePage->getMedia('slider') as $image){
            $sliders[] = '/storage/temps/'.$image->file_name;
        }

        return $sliders;
    }
}
