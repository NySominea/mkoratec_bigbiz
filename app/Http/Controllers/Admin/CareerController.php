<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Career;
use DB;

class CareerController extends Controller
{
    public function index()
    {   
        $careers = Career::orderBy('id','ASC')->paginate(15);
        return view('admin.career.index',compact('careers'));
    }
    public function create()
    {  
        return view('admin.career.add-update');
    }

    public function store(Request $request)
    {   
        $this->validate($request,[
            'title' => 'required'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('careers.index')->withSuccess('You have just added a career successfully!');    
    }

    public function edit($id)
    {
        $career = Career::findOrFail($id);
        return view('admin.career.add-update',compact('career'));
    }

    public function update(Request $request, $id)
    {   
        $this->validate($request,[
            'title' => 'required'
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('careers.index')->withSuccess('You have just updated a career successfully!');    
    }

    public function destroy($id)
    {
        $result = false;
        $item = Career::findOrFail($id);
        if($item){
            DB::beginTransaction();
            try{
                if($item->delete()){
                    $result = true;
                }
                DB::commit();
            }catch(Exception $exception){
                DB::rollback();
                $result = false;
            }
        }
        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        $career = isset($id) ? career::find($id) : new career;
        DB::beginTransaction();
        try{
            if(!$career) return redirect()->back()->withError('There is no record found!');
            $career->fill($data);
            $career->save();
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $career;
    }
}
