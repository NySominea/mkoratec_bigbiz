<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Setting;

class SocialMediaController extends Controller
{
    public function index(){
        $settings = Setting::all()->keyBy('key');
        return view('admin.social-media.index',compact('settings'));
    }
    public function update(){
        foreach(['facebook','instagram','wechat','whatsapp','twitter','line','telegram'] as $key){
            $row = Setting::where('key',$key)->first();
            $r = ['key' => $key, 'value' => request()->{$key}?:''];
            if($row){
                $row->update($r);
            }else{
                Setting::create($r);
            }
        }
        return redirect()->route('settings.social-media.index');
    }
}
