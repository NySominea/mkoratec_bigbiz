<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(15);
        return view('admin.user.index',compact('users'));
    }

    public function create()
     {
         $roles = [0 => 'Choose a role'] + Role::all()->pluck('name','id')->toArray();
         return view('admin.user.add-update',compact('roles'));
     }
 
     public function store(Request $request)
     {
         $this->validate($request,[
             'name' => 'required|unique:users,name',
             'email' => 'required|unique:users,email',
             'password' => 'required|confirmed|min:6',
             'password_confirmation' => 'required|min:6'
         ]);
         
         $this->saveToDB($request->all());
         return redirect()->route('users.index')->withSuccess('You have just added a user successfully!');
     }

     public function edit($id)
     {
         $roles = [0 => 'Choose a role'] + Role::all()->pluck('name','id')->toArray();
         $user = User::findOrFail($id);
         return view('admin.user.add-update',compact('roles','user'));
     }
 
     public function update(Request $request, $id)
     {
         $this->validate($request,[
             'name' => 'required|unique:users,name,'.$id,
             'email' => 'required|unique:users,email,'.$id
         ]);
         
         if($request->password){
             $this->validate($request,[
                 'password' => 'required|confirmed|min:6',
                 'password_confirmation' => 'required',
             ]);
         }
 
         $this->saveToDB($request->all(),$id);
         return redirect()->route('users.index')->withSuccess('You have just updated a user successfully!');
     }

     public function destroy($id)
     {
        $result = false;
        $user = User::find($id);
        if($user){
            DB::beginTransaction();
            try{
                if($user->delete()){
                    $result = true;
                }
                DB::commit();
            }catch(Exception $exception){
                DB::rollback();
                $result = false;
            }
        }
        return response()->json(['success' => $result]);
     }

    public function deleteImage(){
        $result = false;
        $user_id = request()->get('user_id');
        DB::beginTransaction();
        try{
            $user = User::findOrFail($user_id);
            if($user){
                $user->clearMediaCollection('images');
                $result = true;
            }
            DB::commit();
        }catch(Exception $exception){
            DB::rollback();
            $result = false;
        }
        
        return response()->json(['success' => $result]);
    }

     public function saveToDB($data, $id=null){
         DB::beginTransaction();
         try{
             $user = isset($id) ? User::find($id) : new User;
             if(!$user) return redirect()->back()->withError('There is no record found!');
    
             if($data['password']){
                 $data['password'] = Hash::make($data['password']);
             }else{
                 unset($data['password']);
             }
             $user->fill($data);
             if($user->save()){
                $role = Role::find($data['role_id']);
                if($role) $user->syncRoles([$role->name]);

                if(isset($data['image']) && !empty($data['image'])){
                    $user->clearMediaCollection('images'); 
                    $user->addMedia($data['image'])
                         ->toMediaCollection('images');
                }
             }

             DB::commit();
         }catch(Exception $ex){
             DB::rollback();
             return redirect()->back()->withError('There was an error during operation!');
         }
         return $user;
     }
}
