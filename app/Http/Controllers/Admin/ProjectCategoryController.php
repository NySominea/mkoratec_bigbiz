<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProjectCategory;
use DB;

class ProjectCategoryController extends Controller
{
    public function index()
    {
        $categories = ProjectCategory::with('projects')->paginate(15);
        return view('admin.project.category.index',compact('categories'));
    }

    public function create()
    {
        return view('admin.project.category.add-update');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:users,name'
        ]);
        
        $this->saveToDB($request->all());
        return redirect()->route('projects.categories.index')->withSuccess('You have just added a category successfully!');
    }

    public function edit($id)
    {
        $category = ProjectCategory::findOrFail($id);
        return view('admin.project.category.add-update',compact('category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|unique:users,name,'.$id
        ]);

        $this->saveToDB($request->all(),$id);
        return redirect()->route('users.index')->withSuccess('You have just updated a category successfully!');
    }

    public function destroy($id)
    {
        $result = false;
        $category = ProjectCategory::find($id);
        if($category){
            DB::beginTransaction();
            try{
                if($category->delete()){
                    $result = true;
                }
                DB::commit();
            }catch(Exception $exception){
                DB::rollback();
                $result = false;
            }
        }
        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $category = isset($id) ? ProjectCategory::find($id) : new ProjectCategory;
            if(!$category) return redirect()->back()->withError('There is no record found!');

            $category->fill($data);
            $category->save();

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $category;
    }
}
