<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ContactUs;

class ContactUsController extends Controller
{
    
    function index(){
        $contact_us = ContactUs::first() ? : new ContactUs();
        return view('admin.contact_us.index',compact('contact_us'));
    }

    function update(){
        $this->validate(request(),[]); 

        $contact_us = ContactUs::first();
        if(!$contact_us){
            $contact_us = new ContactUs();
        }

        $contact_us->fill(request()->all())->save();
        if(isset(request()->image) && !empty(request()->image)){
            $contact_us->clearMediaCollection('images'); 
            $contact_us->addMedia(request()->image)
                 ->toMediaCollection('images');
        }
        
        return redirect()->back()->withSuccess('Contact us infomation has been updated!');
    }

}
