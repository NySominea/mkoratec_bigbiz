<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AboutUs;
use Redirect;
use URL;
use Validator;

class AboutUsController extends Controller
{
    
    function index(){
        $about_us = AboutUs::first();
        return view('admin.about_us.index',compact('about_us'));
    }

    function updateCompanyProfile(){
        $validator = Validator::make(request()->all(),[
            'background' => 'nullable',
        ],[
            'background.required' => 'image field is required'
        ]);

        if($validator->fails()){
            return Redirect::to(URL::previous() . "#company-description")->withErrors($validator->errors());
        }

        $about_us = AboutUs::first();
        $about_us->fill(request()->all())->save();
        
        if(request()->get('background')){
            $about_us->clearMediaCollection('background');
            $about_us->addMedia(request()->get('background'))->toMediaCollection('background');
        }
        
        return Redirect::to(URL::previous() . "#company-description")->withCompanyDescriptionSuccess('Company vision and mission has been updated!');
    }

    function updateCompanyDescription(){
        $validator = Validator::make(request()->all(),[
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return Redirect::to(URL::previous() . "#company-profile")->withErrors($validator->errors());
        }

        $about_us = AboutUs::first();
        $about_us->fill(request()->all())->save();
        return Redirect::to(URL::previous() . "#company-profile")->withCompanyProfileSuccess('Company description has been updated!');
    }

    function updateHeaderImage(){
        $this->validate(request(),[
            'image' => 'required'
        ]);

        $about_us = AboutUs::first();
        $about_us->clearMediaCollection('header-image');
        $about_us->addMedia(request()->get('image'))->toMediaCollection('header-image');

        return Redirect::to(URL::previous() . "#header-image")->withSuccess('About us information has been updated!');
    }

}
