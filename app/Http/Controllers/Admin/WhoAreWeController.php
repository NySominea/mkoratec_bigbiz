<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ManagementTeam;
use App\Model\WhoWeAre;
use Redirect;
use URL;
use Validator;

class WhoAreWeController extends Controller
{
    function index(){
        $who_we_are = WhoWeAre::first();
        $managers = ManagementTeam::all();
        return view('admin.who_are_we.index',compact('who_we_are','managers'));
    }

    function removeManagementTeam($id){
        $manager = ManagementTeam::findOrFail($id);
        $manager->delete();
        return response()->json([]);
    }

    function update_message(){
        $this->validate(request(),[
            'chairman_name' => 'required',
            'message' => 'required',
            'ceo-image' => 'nullable'
        ]);

        $who_are_we = WhoWeAre::first();
        $who_are_we->fill(request()->all());

        if(request()->get('ceo-image')){
            $who_are_we->clearMediaCollection('ceo-image');
            $who_are_we->addMedia(request()->get('ceo-image'))->toMediaCollection('ceo-image');
        }
        $who_are_we->save();

        return Redirect::to(URL::previous() . "#message")->withMessageSuccess('Message updated!!');
    }

    function saveManagementTeam(){
        $validator = Validator::make(request()->all(),[
            'name' => 'required',
            'position' => 'required',
            'image' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $management_team = ManagementTeam::create(request()->all());
        $management_team->addMedia(request()->get('image'))->toMediaCollection('image');

        return $management_team;
    }

    function getManagementTeam(){
        $id = request()->get('mid');
        $management_team = ManagementTeam::find($id);
        $url = $management_team->getMedia('image')->first()->getUrl();

        return response()->json([
            'id' => $management_team->id,
            'name' => $management_team->name,
            'position' => $management_team->position,
            'url' => $url
        ]);
    }

    function updateManagementTeam(){
        $validator = Validator::make(request()->all(),[
            'id' => 'required',
            'update-name' => 'required',
            'update-position' => 'required',
            'background' => 'nullable'
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $management_team = ManagementTeam::find(request()->get('id'));
        $management_team->name = request()->get('update-name');
        $management_team->position = request()->get('update-position');
        $management_team->save();

        if(request()->get('background')){
            $management_team->clearMediaCollection('image');
            $management_team->addMedia(request()->get('background'))->toMediaCollection('image');
        }

        return $management_team;
    }

    function updateProfile(){
        $validator = Validator::make(request()->all(),[
            'bigbiz_profile' => 'required'
        ]);

        if($validator->fails()){
            return Redirect::to(URL::previous() . "#profile")->withErrors($validator->errors());
        }

        $who_are_we = WhoWeAre::first();
        $who_are_we->fill(request()->all())->save();
        return Redirect::to(URL::previous() . "#profile")->withBigbizSuccess('BigBiz profile updated!!');
    }

}
