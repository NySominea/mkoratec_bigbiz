<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;    
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Session;
use Hash;
use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/admin/dashboard';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
        return view('admin.auth.login-form');
    }

    public function login(Request $request){
        $this->validateLogin($request);
        
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), 1
        );
    }

    public function validateLogin(Request $request)
    {   
        $request->validate([
            'name' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function credentials(Request $request)
    {
        return $request->only('name', 'password');
    }

    public function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            'failed' => [trans('auth.failed')],
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        return redirect('/login');
    }
    public function username()
    {
        return 'name';
    }
}
