<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Project;
use App\Model\ProjectCategory;
use DB;

class ProjectController extends Controller
{
    public function index(){
        $projects = Project::with('category')->orderBy('id','DESC')->paginate(15);
        //  dd($projects);  
        return view('admin.project.index',compact('projects'));
    }

    public function create()
    {
        $categories = ['0' => 'Choose Category'] + ProjectCategory::pluck('name','id')->toArray();
        return view('admin.project.add-update',compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:users,name',
            'description' => 'required'
        ]);
        
        $this->saveToDB($request->all());
        return redirect()->route('projects.index')->withSuccess('You have just added a category successfully!');
    }

    public function edit($id)
    {
        $project = Project::findOrFail($id);
        $categories = ['0' => 'Choose Category'] + ProjectCategory::pluck('name','id')->toArray();
        return view('admin.project.add-update',compact('project','categories'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|unique:users,name,'.$id,
            'description' => 'required'
        ]);

        $this->saveToDB($request->all(),$id);
        return redirect()->route('projects.index')->withSuccess('You have just updated a category successfully!');
    }

    public function destroy($id)
    {
        $result = false;
        $project = Project::find($id);
        if($project){
            DB::beginTransaction();
            try{
                if($project->delete()){
                    $result = true;
                }
                DB::commit();
            }catch(Exception $exception){
                DB::rollback();
                $result = false;
            }
        }
        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $project = isset($id) ? Project::find($id) : new Project;
            if(!$project) return redirect()->back()->withError('There is no record found!');

            $project->fill($data);
            if($project->save()){
                if(isset($data['image']) && !empty($data['image'])){
                    $project->clearMediaCollection('images'); 
                    $project->addMedia($data['image'])
                         ->toMediaCollection('images');
                }
            }

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $project;
    }
}
