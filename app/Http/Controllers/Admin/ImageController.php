<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use File;

class ImageController extends Controller
{

    function uploadTempMultiple(){
        $files = request()->file('file');
        $urls = [];
        foreach($files as $image){
            $extension = $image->getClientOriginalExtension();
            $file_name = time().'.'.$extension;
            Storage::disk('public')->put('temps/'.$file_name,  File::get($image));

            $urls[] = [
                'url' => '/storage/temps/'.$file_name,
                'id' => $file_name,
                'file_path' => 'temps/'.$file_name
            ];
        }

        return $urls;
    }
    
    function uploadImage(){
        $image = request()->file('file');
        $extension = $image->getClientOriginalExtension();
        $file_name = time().'.'.$extension;
        Storage::disk('public')->put('images/'.$file_name,  File::get($image));

        return (Storage::disk('public')->url('images/'.$file_name));
    }

    public function dropzoneSaveTempImage(){
        if(request()->hasFile('file')){
            $path = saveTempImage(request()->file('file'));
            return response()->json(['status' => true, 'path' => $path]);
        }
        return response()->json(['status' => false]);
    }

}
