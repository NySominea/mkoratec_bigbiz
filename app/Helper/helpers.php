<?php



function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function saveTempImage($image){
    $filename = time().'-'.$image->getClientOriginalName();
    $destination = 'app-assets/images/temp';
    
    $image->move(public_path($destination), $filename);
    $path = $destination.'/'.$filename;

    return $path;
}

function saveSummernoteImage($image){
    $filename =time().'-'.$image->getClientOriginalExtension();
    $destination = 'admin/assets/images/summernote';
    
    $image->move(public_path($destination), $filename);
    $path = $destination.'/'.$filename;

    return $path;
}

function permissions(){
    return [
        'projects' => [
            'module' => 'Project',
            'permissions' => [
                ['text' => 'View Project', 'value' => 'view-project'],
                ['text' => 'Project Modification', 'value' => 'project-modification'],
                ['text' => 'Add New Project', 'value' => 'add-new-project'],
                ['text' => 'View Project Category', 'value' => 'view-project-category'],
                ['text' => 'Project Category Modification', 'value' => 'project-category-modification'],
                ['text' => 'Add New Project Category', 'value' => 'add-new-project-category']
            ]
        ],
        'user' => [
            'module' => 'User',
            'permissions' => [
                ['text' => 'View User', 'value' => 'view-user'],
                ['text' => 'User Modification', 'value' => 'user-modification'],
                ['text' => 'Add New User', 'value' => 'add-new-user'],
                ['text' => 'View Role', 'value' => 'view-role'],
                ['text' => 'Role Modification', 'value' => 'role-modification'],
                ['text' => 'Add New Role', 'value' => 'add-new-role'],
            ]
        ],
        'news' => [
            'module' => 'News',
            'permissions' => [
                ['text' => 'View News', 'value' => 'view-news'],
                ['text' => 'News Modification', 'value' => 'news-modification'],
                ['text' => 'Add New News', 'value' => 'add-new-news']
            ]
        ],
        'career' => [
            'module' => 'Careers',
            'permissions' => [
                ['text' => 'View Career', 'value' => 'view-career'],
                ['text' => 'Career Modification', 'value' => 'career-modification'],
                ['text' => 'Add New Career', 'value' => 'add-new-career']
            ]
        ],
        'homepage' => [
            'module' => 'Homepage',
            'permissions' => [
                ['text' => 'Banner Slider', 'value' => 'banner-slider'],
                ['text' => 'Our Story', 'value' => 'our-story'],
                ['text' => 'Video', 'value' => 'video']
            ]
            ],
        'who-we-are-page' => [
            'module' => 'Who we are page',
            'permissions' => [
                ['text' => 'Management Team', 'value' => 'management-team'],
                ['text' => 'Company Profile', 'value' => 'company-profile'],
                ['text' => 'Chairman Message', 'value' => 'chairman-message']
            ]
        ],
        'other-page' => [
            'module' => 'Other Page',
            'permissions' => [
                ['text' => 'About Us Page', 'value' => 'about-us-page'],
                ['text' => 'Contact Us Page', 'value' => 'contact-us-page']
            ]
        ],
        'seo' => [
            'module' => 'SEO',
            'permissions' => [
                ['text' => 'View SEO', 'value' => 'view-seo'],
                ['text' => 'SEO Modification', 'value' => 'seo-modification']
            ]
        ],
        'social-media' => [
            'module' => 'Social Media',
            'permissions' => [
                ['text' => 'View Social Media', 'value' => 'view-socia-media'],
                ['text' => 'Social Media Modification', 'value' => 'social-media-modification']
            ]
        ],
        'page-image' => [
            'module' => 'Page Image',
            'permissions' => [
                ['text' => 'View Page Image', 'value' => 'view-page-image'],
                ['text' => 'Page Image Modification', 'value' => 'page-image-modification']
            ]
        ]
    ];
}