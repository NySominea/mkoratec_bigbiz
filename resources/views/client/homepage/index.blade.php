@extends('client.layouts.master')

@section('title') 
    Big Biz
@endsection

@section('content')
<div id="homepage">

    {{-- Start Banner --}}
    <div class="banner" id="banner">
        <div class="banner-slide">
            <div id="slide" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
                @php $images = isset($sliders) ? $sliders->getMedia('slider') : []; @endphp
                <ul class="carousel-indicators">
                    @foreach($images as $key => $image)
                    <li data-target="#slide" data-slide-to="{{$key}}"></li>
                    @endforeach
                </ul> 
                <div class="carousel-inner">
                    @foreach($images as $key => $image)
                        <div class="carousel-item {{$key==0 ? 'active' : ''}}" style="background-image: url({{$image->getUrl()}})"></div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#slide" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#slide" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
            </div>
        </div>
    </div>
    {{-- End Banner  --}}

    {{-- Start Our Story --}}
    <div class="our-story">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 order-lg-1 pb-4">
                    <div class="title">Disovery Our Story</div>
                    <div class="description">
                        {{ $home_page->discover_story }}
                    </div>
                    <div class="read-more"><a class="btn btn-default" href="{{route('client.who-we-are')}}">READ MORE</a></div>
                </div>
                <div class="col-lg-6 order-lg-21">
                    <img src="{{$home_page->getFirstMedia('background') ? $home_page->getFirstMedia('background')->getUrl() : '/client/images/our-story.jpg'}}" alt="" width="100%">
                </div>
            </div>
        </div>
    </div>
    {{-- End Our Story --}}

    {{-- Start News --}}
    <div class="news">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">News</div>
                </div>
            </div>
            <div class="row">
                @foreach($news as $new)
                <div class="col-lg-3 col-md-6 col-sm-6 mb-3">
                    <div class="card item">
                        @php $image = $new->getMedia('thumbnail')->first() @endphp
                        <img src="{{ $image ? $image->getUrl() : '' }}" class="card-img-top" alt="">
                        <div class="overlay">
                            <div class="title">{{ $new->title }}</div>
                            <div class="read-more mt-4"><a class="text-white" href="{{route('client.news-detail',$new->id)}}"> Read More</a></div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    {{-- End News --}}

    {{-- Start News --}}
    <div class="video">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">Videos</div>
                </div>
            </div>
            <div class="row">
                @foreach(explode(',',$home_page->video_key) as $key)
                <div class="col-lg-6 mb-3">
                    <iframe width="100%" height="300px" src="https://www.youtube.com/embed/{{$key}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    {{-- End News --}}
</div>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function(){
            $('.video').hover(function toggleControls() {
                if (this.hasAttribute("controls")) {
                    this.removeAttribute("controls")
                } else {
                    this.setAttribute("controls", "controls")
                }
            })
        })
    </script>
@endsection