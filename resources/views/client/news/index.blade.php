@extends('client.layouts.master')

@section('title') 
    News | Big Biz
@endsection

@section('meta-tag')
<meta name="description" content="">
<meta name="keywords" content="">
@endsection
@section('content')
<div id="news">
    <div class="page-banner" style="background-image: url('{{ isset($settings['news_image']) && $settings['news_image']->getMedia('header-image')->first() !== null ? $settings['news_image']->getMedia('header-image')->first()->getUrl() : '/client/images/bg.jpg' }}')"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="section-title">News</div>
            </div>
        </div>
        <div class="row">
            @foreach($news as $new)
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 mb-3">
                <div class="card item">
                    @php $image = $new->getMedia('thumbnail')->first() @endphp
                    <img src="{{ $image ? $image->getUrl() : '' }}" class="card-img-top" alt="">
                    <div class="overlay">
                        <div class="title">{{ $new->title }}</div>
                        <div class="read-more mt-4"><a class="text-white" href="{{route('client.news-detail',$new->id)}}"> Read More</a></div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('custom-js')

@endsection