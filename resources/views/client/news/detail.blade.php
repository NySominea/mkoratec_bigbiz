@extends('client.layouts.master')

@section('title') 
    News | Big Biz
@endsection

@section('meta-tag')
<meta name="description" content="">
<meta name="keywords" content="">
@endsection
@section('content')
<div id="news-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-8 main-section pb-3">
                <div class="card p-4">
                    <div class="title">{{ $news->title }}</div>
                    <hr>
                    <div class="description">
                        {!! $news->description !!}
                    </div>
                </div>
                
            </div>
            <div class="col-md-4 side-section">
                <div class="card pt-4 pb-4 pl-3 pr-3">
                    <div class="heading">
                        Latest News
                    </div>
                    @foreach($newsList as $row)
                    <div class="content pt-2 pb-2 border-bottom">
                        <a href="{{ route('client.news-detail',1) }}" class="text-decoration-none item">{{ $row->title }}<i class="fa fa-link"></i></a>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('custom-js')

@endsection