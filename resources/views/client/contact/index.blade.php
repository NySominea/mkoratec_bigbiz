@extends('client.layouts.master')

@section('title') 
    Contact Us | Big Biz
@endsection

@section('meta-tag')
<meta name="description" content="">
<meta name="keywords" content="">
@endsection
@section('content')
<div id="contact">
    <div class="page-banner" style="background-image: url('{{ isset($settings['contact_us_image']) && $settings['contact_us_image']->getMedia('header-image')->first() !== null ? $settings['contact_us_image']->getMedia('header-image')->first()->getUrl() : '/client/images/bg.jpg' }}')"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="section-title">Contact Us</div>
            </div>
        </div>
        <div class="row" style="padding:15px; background-color:white;">
            <div class="col-md-12 main-content">
                <div class="card">
                    <div class="row contact-body">
                        <div class="col-md-6 order-md-12 p-0 info" style="background-image:url('{{ $contactUs->getMedia('images')->first() !== null ? $contactUs->getMedia('images')->first()->getUrl() : '/client/images/bg.jpg' }}')">
                            <div class="overlay"></div>
                            <div class="desc">
                                <div class="heading"><i class="fa fa-map-marker mr-2"></i> Loaction</div>
                                <div class="text">{{ isset($contactUs->address) ? $contactUs->address : '' }}</div>
                                <br>
                                <div class="heading"><i class="fa fa-phone mr-2"></i> Tel</div>
                                <div class="text">{{ isset($contactUs->tel) ? $contactUs->tel : '' }}</div>
                                <br>
                                <div class="heading"><i class="fa fa-phone mr-2"></i> Fax</div>
                                <div class="text">{{ isset($contactUs->fax) ? $contactUs->fax : '' }}</div>
                                <br>
                                <div class="heading"><i class="fa fa-envelope mr-2"></i>Email</div>
                                <div class="text">{{ isset($contactUs->email) ? $contactUs->email : '' }}</div>
                            </div>
                        </div>
                        <div class="col-md-6 order-md-1 p-0 map">
                            <iframe src="{{ $contactUs->map }}" width="100%" height="500" min-width="300" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')

@endsection