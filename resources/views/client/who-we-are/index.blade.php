@extends('client.layouts.master')

@section('title') 
    Who We Are | Big Biz
@endsection

@section('meta-tag')
<meta name="description" content="">
<meta name="keywords" content="">
@endsection
@section('content') 
<div id="who-we-are">
    @php $image = $about_us->getMedia('header-image')->first() @endphp
    <div class="page-banner" style="background-image: url('{{ isset($settings['about_us_image']) && $settings['about_us_image']->getMedia('header-image')->first() !== null ? $settings['about_us_image']->getMedia('header-image')->first()->getUrl() : '/client/images/bg.jpg' }}')"></div>

    <div class="team">
        <div class="main-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="section-title">TEAM</div>
                        <div class="heading-title">BIGBIZ Management Team</div>
                    </div>

                    @foreach($management_teams as $management_team)
                    <div class="col-lg-3 col-md-6 col-sm-6 member">
                        <div class="card item">
                            <a href="#" class="text-decoration-non">
                                @php $image = $management_team->getMedia('image')->first(); @endphp
                                <img src="{{ $image ? $image->getUrl() : '' }}" class="card-img-top" alt="">
                            </a>
                            <div class="card-footer text-center">
                                <div class="name">{{ $management_team->name }}</div>
                                <div class="position">{{ $management_team->position }}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="profile">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-title">Profile</div>
                    <div class="heading-title">BIGBIZ Profile</div>
                </div>
                <div class="col-lg-12">
                    <div class="description">
                        {!! $who_are_we->bigbiz_profile !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="message">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-title">Message</div>
                    <div class="heading-title">Message from CEO/Chairman</div>
                </div>
                <div class="rounded image img-circle my-auto col-lg-4 col-md-5 order-md-1">
                    @php $image = $who_are_we->getMedia('ceo-image')->first(); @endphp
                    <img src="{{ $image ? $image->getUrl() : '/client/images/profile1.png' }}" width="100%" class="rounded">
                </div>
                <div class="col-lg-6 offset-lg-1 col-md-7 order-md-21">
                    <div class="description">
                        <h4>{{ $who_are_we->chairman_name }}</h4>
                        <p>  {{ $who_are_we->message }}   
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')

@endsection