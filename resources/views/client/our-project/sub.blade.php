@extends('client.layouts.master')

@section('title') 
    {{ $projectCategories[$id]->name }} Projects | Big Biz
@endsection

@section('meta-tag')
<meta name="description" content="">
<meta name="keywords" content="">
@endsection

@section('content')
<div id="project-category">
    <div class="page-banner" style="background-image: url(/client/images/bg.jpg)"></div>

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-title">Projects</div>
                    <div class="heading-title">{{ $projectCategories[$id]->name }} Projects</div>
                </div>
            </div>
            @foreach($projectCategories[$id]->projects as $row)
            <div class="row item mb-5">
                <div class="col-lg-7 col-md-6">
                    @php $image = $row->getMedia('images')->first(); @endphp
                    <div class="image" style="background-image: url({{$image ? $image->getUrl() : ''}})">
                    </div>
                </div>
                <div class="col-lg-5 col-md-6">
                    <div class="info shadow">
                        <div class="title">{{ $row->name }}</div>
                        <div class="description">{{ $row->description }}</div>
                        @if($row->client)<div>Client: {{ $row->client }}</div>@endif
                        @if($row->location)<div>Location: {{ $row->location }}</div>@endif
                        @if($row->year)<div>Year Completed: {{ $row->year }}</div>@endif
                        @if($row->value)<div>Value: {{ $row->value }}</div>@endif
                        @if($row->architect)<div>Architect: {{ $row->architect }}</div>@endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function(){
            resizeItemThumbnail();
            $(window).resize(function(){
                resizeItemThumbnail();
            })
            function resizeItemThumbnail(){
                let height = $(window).outerWidth(); console.log(height)
                if(height > 767){
                    $('.item').each(function(){
                        $(this).find('.image').css('height',$(this).find('.info').outerHeight() + 'px')
                    })
                }else if(height > 400) {
                    $('.item').each(function(){
                        $(this).find('.image').css('height','250px')
                    })
                }else{
                    $('.item').each(function(){
                        $(this).find('.image').css('height','200px')
                    })
                }
            }
        })
    </script>
@endsection