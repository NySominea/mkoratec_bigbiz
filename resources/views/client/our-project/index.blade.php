@extends('client.layouts.master')

@section('title') 
    Our Projects | Big Biz
@endsection

@section('meta-tag')
<meta name="description" content="">
<meta name="keywords" content="">
@endsection

@section('content')
<div id="our-project">
    <div class="page-banner" style="background-image: url('{{ isset($settings['project_image']) && $settings['project_image']->getMedia('header-image')->first() !== null ? $settings['project_image']->getMedia('header-image')->first()->getUrl() : '/client/images/bg.jpg' }}')"></div>

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-title">Projects</div>
                    <div class="heading-title">Our Projects</div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-projects flex-column flex-sm-row" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="true">All</a>
                        </li> 
                        @foreach($projectCategories as $key => $row)
                            <li class="nav-item">
                                <a class="nav-link" id="pills-{{$key}}-tab" data-toggle="pill" href="#pills-{{$key}}" role="tab" aria-controls="pills-{{$key}}" aria-selected="false">{{$row->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
                            <div class="row">
                                @foreach($projects as $row)
                                <div class="col-lg-4 col-md-6 col-sm-6 mb-4">
                                    @php $image = $row->getMedia('images')->first(); @endphp
                                    <div class="card item" style="background-image:url({{ $image ? $image->getUrl() : '' }})">
                                        <div class="overlay">
                                            <div class="title h-100 my-auto"><a href="#" class="text-decoration-none text-white">{{ $row->name }}</a></div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @foreach($projectCategories as $key => $row)
                            <div class="tab-pane fade" id="pills-{{$key}}" role="tabpanel" aria-labelledby="pills-{{$key}}-tab">
                                <div class="row">
                                    @foreach($row->projects as $key => $row)
                                    <div class="col-lg-4 col-md-6 col-sm-6 mb-4">
                                        @php $image = $row->getMedia('images')->first(); @endphp
                                        <div class="card item" style="background-image:url({{ $image ? $image->getUrl() : '' }})">
                                            <div class="overlay">
                                                <div class="title h-100 my-auto"><a href="#" class="text-decoration-none text-white">{{ $row->name }}</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')
    <script>
    </script>
@endsection