<div class="footer">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="title">THE BIG BIZ</div>
                    <div class="description">
                        The BIG BIZ is one of the largest investment companies in Cambodia and is reconginsed as the country's most dynamic
                        and diversified business conglomerate.
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="title">OTHER PAGES</div>
                    <div class="description">
                        <ul class="list-group list-unstyled">
                            <li><a href="{{route('client.about-us')}}">About Us</a></li>
                            <li><a href="{{route('client.who-we-are')}}">Who We Are</a></li>
                            <li><a href="{{route('client.our-projects')}}" >Our Projects</a></li>
                            <li><a href="{{route('client.career-opportunities')}}">Careers</a></li>
                            <li><a href="{{route('client.contact')}}">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="title">CONTACT US</div>
                    <div class="description">
                        <div>Email: {{ isset($contactUs->email) ? $contactUs->email : '' }}</div>
                        <div>Tel: {{ isset($contactUs->tel) ? $contactUs->tel : ''}}</div>
                        <div>Fax: {{ isset($contactUs->fax) ? $contactUs->fax : ''}}</div>
                        <div>Address: {{ isset($contactUs->address) ? $contactUs->address : ''}}</div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 text-center">
                    <img src="/client/images/BigBiz1.png" alt="" width="100%" style="max-width:200px;">
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-12">
                    <div class="text-center">Follow Us</div>
                    <div class="text-center mt-3">
                        <a target="_BLANK" href="{{isset($settings,$settings['facebook']) ? $settings['facebook']->value : '#' }}" class="mr-2"><i class="fab fa-facebook fa-2x text-secondary"></i></a>
                        <a target="_BLANK" href="{{isset($settings,$settings['telegram']) ? $settings['telegram']->value : '#' }}" class="mr-2"><i class="fab fa-telegram fa-2x text-secondary"></i></a>
                        <a target="_BLANK" href="{{isset($settings,$settings['instagram']) ? $settings['instagram']->value : '#' }}" class="mr-2"><i class="fab fa-instagram fa-2x text-secondary"></i></a>
                        <a target="_BLANK" href="{{isset($settings,$settings['wechat']) ? $settings['wechat']->value : '#' }}" class="mr-2"><i class="fab fa-weixin fa-2x text-secondary"></i></a>
                        <a target="_BLANK" href="{{isset($settings,$settings['whatsapp']) ? $settings['whatsapp']->value : '#' }}" class="mr-2"><i class="fab fa-whatsapp fa-2x text-secondary"></i></a>
                        <a target="_BLANK" href="{{isset($settings,$settings['line']) ? $settings['line']->value : '#' }}" class="mr-2"><i class="fab fa-line fa-2x text-secondary"></i></a>
                        <a target="_BLANK" href="{{isset($settings,$settings['twitter']) ? $settings['twitter']->value : '#' }}" class="mr-2"><i class="fab fa-twitter fa-2x text-secondary"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>