<nav id="navbar" class="navbar navbar-expand-lg navbar-light animated shadow">
    <div class="container">
        {{-- @php $image = !isset($settings['company'][$constants['MAIN_LOGO']]) ? $settings['company'][$constants['MAIN_LOGO']]->getFirstMedia('images') : null;  @endphp --}}
        <a class="navbar-brand" href="{{url('/')}}"><img src="/client/images/BigBiz.png"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('client.homepage')}}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown {{ in_array(Route::currentRouteName(), ['client.about-us','clent.who-we-are']) ? ' active' : ''}}">
                    <a class="nav-link dropdown-toggle {{ in_array(Route::currentRouteName(), ['client.about-us','client.who-we-are']) ? ' active' : ''}}" data-test="1" href="{{route('client.who-we-are')}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      About Us
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('client.about-us')}}">Vision & Mission</a>
                        <a class="dropdown-item" href="{{route('client.who-we-are')}}">Management Team</a>
                    </div>
                </li>
                <li class="nav-item dropdown {{ in_array(Route::currentRouteName(), ['client.project-category']) ? ' active' : ''}}">
                    <a class="nav-link dropdown-toggle {{ in_array(Route::currentRouteName(), ['client.project-category']) ? ' active' : ''}}" data-test="1" href="{{route('client.our-projects')}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Projection
                    </a>
                    @if(isset($projectCategories) && $projectCategories->count() > 0)
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach($projectCategories as $row)
                            <a class="dropdown-item" href="{{route('client.project-category',$row->id)}}">{{ $row->name }}</a>
                        @endforeach
                    </div>
                    @endif
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ in_array(Route::currentRouteName(), ['client.news-detail']) ? ' active' : ''}}" href="{{route('client.news')}}">News</a>
                </li>
                <li class="nav-item {{ in_array(Route::currentRouteName(), ['client.career-detail']) ? ' active' : ''}}">
                    <a class="nav-link {{ in_array(Route::currentRouteName(), ['client.career-detail']) ? ' active' : ''}}" href="{{route('client.career-opportunities')}}">Careers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('client.contact')}}">Contact</a>
                </li>
                {{-- <li class="nav-item">
                    <div class="nav-link" >
                        <a href="#contact-us" style="margin-right:10px;"><img src="{{asset('client/images/en.jpeg')}}" width="35px"></a>
                        <a href="#contact-us"><img src="{{asset('client/images/zh.jpeg')}}" width="35px"></a>
                    </div>
                </li> --}}
            </ul>
        </div>
    </div>
</nav>