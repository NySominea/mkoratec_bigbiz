@extends('client.layouts.master')

@section('title') 
    {{ $career->title }} - Careers | Big Biz
@endsection

@section('meta-tag')
<meta name="description" content="">
<meta name="keywords" content="">
@endsection
@section('content')
<div id="career-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-8 main-section pb-3">
                <div class="card p-4">
                    <div class="title">{{ $career->title }}</div>
                    <hr>
                    <div class="description">
                        <div class="row mb-4">
                            <div class="col-lg-5">
                                <table>
                                    <tbody><tr>
                                        <td><b>Level</b></td>
                                        <td>{{ $career->level }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Experience</b></td>
                                        <td>{{ $career->experience }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Number</b></td>
                                        <td>{{ $career->number }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Salary</b></td>
                                        <td>{{ $career->salary }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Start Date</b></td>
                                        <td>{{ date('Y-m-d',strtotime($career->started_date)) }}</td>
                                    </tr>
                                </tbody></table>
                            </div>
                            <div class="col-lg-7">
                                <table>
                                    <tbody><tr>
                                        <td><b>Function</b></td>
                                        <td>{{ $career->function }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Qualification</b></td>
                                        <td>{{ $career->qualification }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Language</b></td>
                                        <td>{{ $career->language }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Loaction</b></td>
                                        <td>{{ $career->location }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Expired Date</b></td>
                                        <td>{{ date('Y-m-d',strtotime($career->expired_date)) }}</td>
                                    </tr>
                                </tbody></table>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-md-12">
                                <div class="jd">
                                    {!! $career->description !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-md-4 side-section">
                <div class="card pt-4 pb-4 pl-3 pr-3">
                    <div class="heading">
                        New Careers
                    </div>
                    @foreach($newCareers as $row)
                    <div class="content pt-2 pb-2 border-bottom">
                        <a href="{{ route('client.career-detail',$row->id) }}" class="text-decoration-none item">{{ $row->title }} <i class="fa fa-link"></i></a>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('custom-js')

@endsection