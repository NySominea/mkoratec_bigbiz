@extends('client.layouts.master')

@section('title') 
    Careers | Big Biz
@endsection

@section('meta-tag')
<meta name="description" content="">
<meta name="keywords" content="">
@endsection
@section('content')
<div id="career">
    <div class="page-banner" style="background-image: url('{{ isset($settings['career_image']) && $settings['career_image']->getMedia('header-image')->first() !== null ? $settings['career_image']->getMedia('header-image')->first()->getUrl() : '/client/images/bg.jpg' }}')"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="section-title">Careers</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card p-4">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>#</th>
                                        <th style="min-width:200px;">Position</th>
                                        <th style="min-width:400px;" class="text-center">Description</th>
                                        <th style="min-width:130px;">Location</th>
                                        <th>Candidates</th>
                                        <th style="min-width:10px;">Expired Date</th>
                                    </tr>
                                    @php $i = 1; @endphp
                                    @forelse($careers as $key => $row)
                                    <tr>
                                        <td>{{ $i+$key }}</td>
                                        <td><a href="{{route('client.career-detail',$row->id)}}" class="text-decoration-none text-dark">{{ $row->title }} <i class="fa fa-link"></i></a></td>
                                        <td class="desc">{{ $row->short_description }}</td>
                                        <td>{{ $row->location }}</td>
                                        <td>{{ $row->number }}</td>
                                        <td>{{ date('Y-m-d',strtotime($row->expired_date)) }}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td align="middle" colspan="6" style="padding-top: 20px; padding-bottom: 20px">There is no career available at the moment</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')

@endsection