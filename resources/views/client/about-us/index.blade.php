@extends('client.layouts.master')

@section('title') 
    About Us | Big Biz
@endsection

@section('meta-tag')
<meta name="description" content="">
<meta name="keywords" content="">
@endsection
@section('content')
<div id="about-us">
    <div class="page-banner" style="background-image: url('{{ isset($settings['about_us_image']) && $settings['about_us_image']->getMedia('header-image')->first() !== null ? $settings['about_us_image']->getMedia('header-image')->first()->getUrl() : '/client/images/bg.jpg' }}')"></div>

    <div class="about-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">{{ $about_us->title}}</div>
                    <div class="description">
                        <p>{{ $about_us->description }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vision-mission">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">Our Vision & Mission</div>
                    @php $image = $about_us->getMedia('background')->first() @endphp
                    <div class="vision" style="overflow: hidden; background-image: url('{{ $image ? $image->getUrl() : '/client/images/bg.jpg' }}')">
                        {{-- <div class="overlay">
                            <div class="row h-100">
                                
                            </div>
                            
                        </div> --}}
                    </div>
                    <div class="mission">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 my-auto">
                                <div class="description">
                                    <h4>Our Mission</h4>
                                    {!! $about_us->company_mission !!}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 my-auto">
                                <div class="description">
                                    <h4>Our Vision</h4>
                                    <p>{{ $about_us->company_vision }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('custom-js')

@endsection