@extends('admin.layouts.master')

@section('content')
@include('common.success_alert')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"> 
                <h4 class="card-title">News</h4>
                <div class="heading-elements">
                    <a href="{{ route('news.create') }}" class="btn btn-icon btn-md btn-success">
                        <i class="ft-plus white"><strong>Add News</strong></i>
                    </a>
                </div>
            </div> 
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <table class="table table-striped">
                            <thead>
                                <th style="width: 25%"></th>
                                <th style="width: 25%">Title</th>
                                <th style="width: 25%">Created At</th>
                                <th style="width: 25%">Action</th>
                            </thead>
                            <tbody>
                                @forelse($news as $new)
                                    <tr>
                                        <td><img src="{{ $new->getMedia('thumbnail')->first()->getUrl() }}" class="height-50" alt="Card image"></td>
                                        <td>{{ $new->title }}</td>
                                        <td>{{ \Carbon\Carbon::parse($new->created_at)->format('Y-m-d h:i A') }}</td>
                                        <td>
                                            <a href="{{ route('news.edit',$new->id) }}"class="btn btn-sm btn-primary btn-outline">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a data-id="{{ $new->id }}" href="javascript:void(0)" class="confirm btn btn-sm btn-danger btn-outline">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty 
                                    <tr> 
                                        <td colspan="4" align="middle" style="padding: 20px !important">No Data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $news->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-script')
<script>
    $(document).ready(function(){
        $('.confirm').click(function(){
            var button = $(this);
            var id = button.attr('data-id')
            swal({
                title: "Are you sure?",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No, cancel!",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Yes, delete it!",
                        value: true,
                        visible: true,
                        className: "btn-danger",
                        closeModal: true
                    }
                }
            }).then(isConfirm => {
                if (isConfirm) {
                    $.ajax({
                        url: '/admin/news/'+id,
                        method: 'delete',
                        dataType: 'json',
                        success: function(response){
                            window.location.reload()
                        }
                    })
                }
            });
        });
    })
</script>
@endsection