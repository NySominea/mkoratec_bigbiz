@extends('admin.layouts.master')

@section('content')
<div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">News</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('news.index') }}">All News</a>
                        </li>
                        <li class="breadcrumb-item active">{{ isset($news) ? 'Modify' : 'Add'}} News
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <div class="btn-group" role="group">
                    <button class="btn btn-outline-primary dropdown-toggle dropdown-menu-right" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ft-settings icon-left"></i> Settings</button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1"><a class="dropdown-item" href="card-bootstrap.html">Bootstrap Cards</a><a class="dropdown-item" href="component-buttons-extended.html">Buttons Extended</a></div>
                </div><a class="btn btn-outline-primary" href="full-calender-basic.html"><i class="ft-mail"></i></a><a class="btn btn-outline-primary" href="timeline-center.html"><i class="ft-pie-chart"></i></a>
            </div>
        </div>
    </div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"> 
                <h4 class="card-title">{{ isset($news) ? 'Modify' : 'Add'}} News</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('common.success_alert')
                    {!! Form::open(['method' => isset($news) ? 'patch' : 'post','url' => isset($news) ? route('news.update',$news->id) : route('news.store')]) !!}
                    <div class="row">
                        <div class="col-xl-8 col-lg-6 col-md-12 mb-1">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="form-group">
                                        <label for="basicInput">Title <span class="required">*</span></label>
                                        {!! Form::text('title',isset($news) ? $news->title : null,['class' => 'form-control']) !!}
                                        @component('common.error_helper_text',['key' => "title"]) 
                                        @endcomponent
                                    </fieldset>
                                </div>

                                <div class="col-md-12">
                                    <fieldset class="form-group">
                                        <label for="basicInput">Description <span class="required">*</span></label>
                                        @component('common.error_helper_text',['key' => "description"]) 
                                        @endcomponent
                                        <textarea id="summernote" name="description">
                                            {{ isset($news) ? $news->description : old('description') }}
                                        </textarea>
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary float-right">{{ isset($news) ? 'Update' : 'Create' }}</button>
                                    <a href="" class="btn btn-danger">Discard</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-12 mb-1">
                            <fieldset>
                                <label>Thumbnail Image (Aspec Ratio 1:1)</label>
                                @component('common.error_helper_text',['key' => "description"]) 
                                @endcomponent
                                @component('common.single_dropzone',
                                    ['id' => "singleDropzone",
                                    'object' => isset($news) ? $news : null, 
                                    'width' => 200, 
                                    'height' => 150,
                                    'imageId' => 'image',
                                    'collection_key' => 'thumbnail'
                                    ])
                                @endcomponent
                            </fieldset>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<script>
$(document).ready(function() {
    $('#summernote').summernote({
        dialogsInBody: true,
            callbacks: {
                onInit:function(){
                    $('body > .note-popover').hide();
                },
                onImageUpload: function(files, editor, welEditable) {
                    sendFile(files[0], editor, welEditable);
                }
            }
        });

  function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "post",
                url: "/admin/upload-image",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    var image = $('<img>').attr('src', url);
                    $('#summernote').summernote("insertNode", image[0]);
                }
            });
        }
});
</script>
@endsection