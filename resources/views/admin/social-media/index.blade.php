@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">SEO</h4>
            </div>

            {{ Form::model($settings,['route' => ['settings.social-media.update'], 'method' => 'POST']) }}
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="basicInput">Facebook</label>
                                {!! Form::text('facebook',isset($settings,$settings['facebook']) ? $settings['facebook']->value : '',['class' => 'form-control', 'placeholder' => 'Enter Facebook URL']) !!}
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Telegram</label>
                                {!! Form::text('telegram',isset($settings,$settings['telegram']) ? $settings['telegram']->value : '',['class' => 'form-control', 'placeholder' => 'Enter Telegram URL']) !!}
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Instagram</label>
                                {!! Form::text('instagram',isset($settings,$settings['instagram']) ? $settings['instagram']->value : '',['class' => 'form-control', 'placeholder' => 'Enter Instagram URL']) !!}
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Whatsapp</label>
                                {!! Form::text('whatsapp',isset($settings,$settings['whatsapp']) ? $settings['whatsapp']->value : '',['class' => 'form-control', 'placeholder' => 'Enter Whatsapp URL']) !!}
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="basicInput">WeChat</label>
                                {!! Form::text('wechat',isset($settings,$settings['wechat']) ? $settings['wechat']->value : '',['class' => 'form-control', 'placeholder' => 'Enter WeChat URL']) !!}
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Line</label>
                                {!! Form::text('line',isset($settings,$settings['line']) ? $settings['line']->value : '',['class' => 'form-control', 'placeholder' => 'Enter Line URL']) !!}
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Twitter</label>
                                {!! Form::text('twitter',isset($settings,$settings['twitter']) ? $settings['twitter']->value : '',['class' => 'form-control', 'placeholder' => 'Enter Twitter URL']) !!}
                            </fieldset>
                        </div>
                    </div>
                </div>
                @can('social-media-modification')
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-primary">Save changes</button>
                </div>
                @endcan
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
