@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Careers</h4>
                @can('add-new-career')
                <div class="heading-elements">
                    <a href="{{route('careers.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th style="min-width:150px;">Career Title</th>
                                <th style="min-width:400px;">Description</th>
                                <th>No. Candidate</th>
                                <th style="min-width:150px;">Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($careers) && $careers->count() > 0) 
                                        @foreach($careers as $key => $row)
                                        <tr>
                                            <td>{{($careers->perPage() * ($careers->currentPage() - 1)) + $key + 1}}</td>
                                            <td>{{ $row->title }}</td>
                                            <td>{{ $row->short_description }}</td>
                                            <td>{{ $row->number }}</td>
                                            <td class="group-btn-action">
                                                @canany(['career-modification'])
                                                    <div class="btn-group btn-group-sm" career="group" aria-label="Basic example">
                                                        <a href="{{ route('careers.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                        <button type="button" class="btn btn-outline-danger delete" data-route="{{route('careers.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                    </div>
                                                @endcanany
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else 
                                        <tr><td colspan="5">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($careers) && $careers->count() > 0)
            <div class="card-footer">
                @if($careers->hasMorePages())
                    <div class="mb-2">
                        {!! $careers->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$careers->firstItem()}} to {{$careers->lastItem()}}
                    of  {{$careers->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
