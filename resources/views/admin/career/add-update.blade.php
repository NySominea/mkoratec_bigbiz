@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($career) ? 'Update' : 'Add' }} Career</h4>
            </div>

            @if(isset($career))
            {{ Form::model($career,['route' => ['careers.update',$career->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'careers.store', 'method' => 'POST']) }}
            @endif
            @csrf

            @php
                $columns = ['title', 'level','experience','number','salary','function','qualification','language', 'location'];
            @endphp
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        @foreach($columns as $column)
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label for="basicInput">Career {{ $column }}</label>
                                {!! Form::text($column,null,['class' => 'form-control', 'placeholder' => 'Enter career '.$column]) !!}
                                @component('common.error_helper_text',['key' => $column])@endcomponent
                            </fieldset>
                        </div>
                        @endforeach
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="basicInput">Start Date</label>
                                {!! Form::date('started_date',isset($career,$career->started_date) ? date('Y-m-d',strtotime($career->started_date)): null,['class' => 'form-control']) !!}
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="basicInput">Expire Date</label>
                                {!! Form::date('expired_date',isset($career) ? date('Y-m-d',strtotime($career->expired_date)): null,['class' => 'form-control']) !!}
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="form-group">
                                <label for="basicInput">Short description</label>
                                {!! Form::textarea('short_description',null,['class' => 'form-control', 'placeholder' => 'Enter career short description', 'rows' => 10]) !!}
                                @component('common.error_helper_text',['key' => 'short_description'])@endcomponent
                            </fieldset>
                        </div>
                        <div class="col-md-8">
                            <fieldset class="form-group">
                                <label for="basicInput">Description</label>
                                {!! Form::textarea('description',null,['id' => 'summernote', 'class' => 'form-control', 'placeholder' => 'Enter career description']) !!}
                                @component('common.error_helper_text',['key' => 'description'])@endcomponent
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{route('careers.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                    <button type="submit" class="btn btn-outline-primary">Save changes</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@section('custom-js')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<script>
$(document).ready(function() {
    $('#summernote').summernote({
        height:200,
        dialogsInBody: true,
        callbacks: {
            onInit:function(){
                $('body > .note-popover').hide();
            },
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        }
    });

  function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: "post",
            url: "/admin/upload-image",
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                var image = $('<img>').attr('src', url);
                $('#summernote').summernote("insertNode", image[0]);
            }
        });
    }
});
</script>
@endsection