@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Projects</h4>
                @can('add-new-project-category')
                <div class="heading-elements">
                    <a href="{{route('projects.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th>Thumbnail</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($projects) && $projects->count() > 0) 
                                        @foreach($projects as $key => $row)
                                        @php $image = $row->getMedia('images')->first() @endphp
                                        <tr>
                                            <td>{{($projects->perPage() * ($projects->currentPage() - 1)) + $key + 1}}</td>
                                            @if($image)
                                                <td> <img width="80px;" src="{{$image->geturl()}}"></td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>{{ $row->name }}</td>
                                            <td>{{ $row->category ? $row->category->name : '' }}</td>
                                            <td class="group-btn-action">
                                                @canany(['project-modification'])
                                                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                        <a href="{{ route('projects.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                        <button type="button" class="btn btn-outline-danger delete" data-route="{{route('projects.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                    </div>
                                                @endcanany
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else 
                                        <tr><td colspan="4">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($projects) && $projects->count() > 0)
            <div class="card-footer">
                @if($projects->hasMorePages())
                    <div class="mb-2">
                        {!! $projects->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$projects->firstItem()}} to {{$projects->lastItem()}}
                    of  {{$projects->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
