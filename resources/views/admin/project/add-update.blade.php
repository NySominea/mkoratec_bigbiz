@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($project) ? 'Update' : 'Add' }} Project</h4>
            </div>

            @if(isset($project))
            {{ Form::model($project,['route' => ['projects.update',$project->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'projects.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="basicInput">Project Name</label>
                                {!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Enter project name']) !!}
                                @component('common.error_helper_text',['key' => "name"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Description</label>
                                {!! Form::textarea('description',null,['class' => 'form-control', 'placeholder' => 'Enter project description','rows' => 5]) !!}
                                @component('common.error_helper_text',['key' => "description"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label>Thumbnail Image (Aspec Ratio 3:2)</label>
                                @component('common.single_dropzone',
                                    ['id' => "projectDropzone",'object' => isset($project) ? $project : null, 'width' => 300, 'height' => 200]) 
                                @endcomponent
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="">Category</label>
                                {!! Form::select("category_id",$categories, isset($project,$project->category) ? $project->category->id : null,['class' => 'custom-select block']) !!}
                                @component('common.error_helper_text',['key' => "category_id"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Client</label>
                                {!! Form::text('client',null,['class' => 'form-control', 'placeholder' => 'Enter client']) !!}
                                @component('common.error_helper_text',['key' => "client"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Location</label>
                                {!! Form::text('location',null,['class' => 'form-control', 'placeholder' => 'Enter location']) !!}
                                @component('common.error_helper_text',['key' => "location"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Year Completed</label>
                                {!! Form::text('year',null,['class' => 'form-control', 'placeholder' => 'Enter year']) !!}
                                @component('common.error_helper_text',['key' => "year"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Value</label>
                                {!! Form::text('value',null,['class' => 'form-control', 'placeholder' => 'Enter value']) !!}
                                @component('common.error_helper_text',['key' => "value"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Architect</label>
                                {!! Form::text('architect',null,['class' => 'form-control', 'placeholder' => 'Enter architect']) !!}
                                @component('common.error_helper_text',['key' => "architect"])@endcomponent
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{route('projects.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                    <button type="submit" class="btn btn-outline-primary">Save changes</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
