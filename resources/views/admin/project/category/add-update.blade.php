@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($category) ? 'Update' : 'Add' }} Project Category</h4>
            </div>

            @if(isset($category))
            {{ Form::model($category,['route' => ['projects.categories.update',$category->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'projects.categories.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="basicInput">Category Name</label>
                                {!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Enter category name']) !!}
                                @component('common.error_helper_text',['key' => "name"])@endcomponent
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{route('projects.categories.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                    <button type="submit" class="btn btn-outline-primary">Save changes</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
