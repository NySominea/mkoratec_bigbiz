@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Project Categories</h4>
                @can('add-new-project-category')
                <div class="heading-elements">
                    <a href="{{route('projects.categories.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th>Name</th>
                                <th>Number of Projects</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($categories) && $categories->count() > 0) 
                                        @foreach($categories as $key => $row)
                                        <tr>
                                            <td>{{($categories->perPage() * ($categories->currentPage() - 1)) + $key + 1}}</td>
                                            <td>{{ $row->name }}</td>
                                            <td>{{ $row->projects->count() }}</td>
                                            <td class="group-btn-action">
                                                @canany(['project-category-modification'])
                                                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                        <a href="{{ route('projects.categories.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                        <button type="button" class="btn btn-outline-danger delete" data-route="{{route('projects.categories.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                    </div>
                                                @endcanany
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else 
                                        <tr><td colspan="4">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($categories) && $categories->count() > 0)
            <div class="card-footer">
                @if($categories->hasMorePages())
                    <div class="mb-2">
                        {!! $categories->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$categories->firstItem()}} to {{$categories->lastItem()}}
                    of  {{$categories->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
