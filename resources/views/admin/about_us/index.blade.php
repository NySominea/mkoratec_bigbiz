@extends('admin.layouts.master')
@section('content')
{{-- {!! Form::model($about_us,['route' => 'aboutUs.updateHeaderImage','method' => 'patch']) !!}
<div class="row" id="header-image">
    <div class="col-md-12 col-sm-12">
        <div class="card" >
            <div class="card-header">
                <h4 class="card-title" id="heading-buttons1">Header Image</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <button type="submit" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-save white"></i>
                    </button>
                </div>
            </div>
            <div class="card-content" style="padding: 20px">
                <section class="row">
                    <div class="col-md-12">
                        @component(
                            'common.single_dropzone',
                            [
                                'id' => "singleDropzone",
                                'object' => isset($about_us) ? $about_us : null, 
                                'width' => 200, 
                                'height' => 150,
                                'collection_key' => 'header-image',
                                'imageId' => 'image'
                            ])
                        @endcomponent 
                        @component('common.error_helper_text',['key' => "image"]) 
                        @endcomponent
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} --}}
{!! Form::model($about_us,['route' => 'aboutUs.updateCompanyProfile','method' => 'patch']) !!}
<div class="row" id="company-profile">
    <div class="col-md-12 col-sm-12">
        <div class="card" >
            <div class="card-header">
                <h4 class="card-title" id="heading-buttons1">Company Description</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <button type="submit" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-save white"></i>
                    </button>
                </div>
            </div>
            <div class="card-content" style="padding: 20px">
                    @component('common.success_alert',['key' => 'company_profile_success'])
                    @endcomponent
                <section class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label for="basicInput">Title: </label>
                            {!! Form::text('title',null,['class' => 'form-control']) !!}
                            @component('common.error_helper_text',['key' => "title"]) 
                            @endcomponent
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label for="basicInput">Description: </label>
                            <textarea name="description" class="form-control">{{$about_us->description}}</textarea>
                            @component('common.error_helper_text',['key' => "description"]) 
                            @endcomponent
                        </fieldset>
                    </div>
                </section>
                
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
{!! Form::model($about_us,['route' => 'aboutUs.updateCompanyProfile','method' => 'patch']) !!}
<div class="row" id="company-description">
    <div class="col-md-12 col-sm-12">
        <div class="card" >
            <div class="card-header">
                <h4 class="card-title" id="heading-buttons1">Company Vision and Mission</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <button type="submit" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-save white"></i>
                    </button>
                </div>
            </div>
            <div class="card-content" style="padding: 20px">
                    @component('common.success_alert',['key' => 'company_description_success'])
                    @endcomponent
                <section class="row">
                    <div class="col-md-8">
                        <fieldset class="form-group">
                            <label for="basicInput">Vision: </label>
                            {!! Form::textarea('company_vision',$about_us->company_vision,['class' => 'form-control']) !!}
                            @component('common.error_helper_text',['key' => "company_vision"]) 
                            @endcomponent
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="basicInput">Mission: </label>
                            {!! Form::textarea('company_mission',$about_us->company_mission,['class' => 'form-control','id' => 'summernote']) !!}
                            @component('common.error_helper_text',['key' => "company_mission"]) 
                            @endcomponent
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="form-group">
                                <label for="basicInput">Mission: </label>
                                @component(
                                    'common.single_dropzone',
                                    [
                                        'id' => "potato",
                                        'object' => isset($about_us) ? $about_us : null, 
                                        'width' => 200, 
                                        'height' => 150,
                                        'collection_key' => 'background',
                                        'imageId' => 'background'
                                    ])
                                @endcomponent
                        </fieldset>
                    </div>
                </section>
                
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection

@section('custom-js')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        dialogsInBody: true,
            callbacks: {
                onInit:function(){
                    $('body > .note-popover').hide();
                }
            }
        });

        if(window.location.hash) {
            $('html,body').animate({
                scrollTop: $(window.location.hash).offset().top - 70
            }, 1000);
        } else {
            
        }
});
</script>
@endsection