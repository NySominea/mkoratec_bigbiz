@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Page Header Image</h4>
            </div>

            {{ Form::model($settings,['route' => ['settings.page-image.update'], 'method' => 'POST']) }}
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label class=" mb-0">Page About Us (Aspect Ratio 2:1)</label>
                                <p style="color:red !important"><small id="error-image"></small></p>
                                @component('common.single_dropzone',
                                    ['id' => "aboutImageDropzone",
                                    'object' =>  isset($settings,$settings['about_us_image']) ? $settings['about_us_image'] : null, 
                                    'width' => 300, 
                                    'height' => 150,
                                    'imageId' => 'about_us_image',
                                    'collection_key' => 'header-image'
                                    ])
                                @endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label class=" mb-0">Page Contact Us (Aspect Ratio 2:1)</label>
                                <p style="color:red !important"><small id="error-image"></small></p>
                                @component('common.single_dropzone',
                                    ['id' => "contactImageDropzone",
                                    'object' =>  isset($settings,$settings['contact_us_image']) ? $settings['contact_us_image'] : null, 
                                    'width' => 300, 
                                    'height' => 150,
                                    'imageId' => 'contact_us_image',
                                    'collection_key' => 'header-image'
                                    ])
                                @endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label class=" mb-0">Page Projection (Aspect Ratio 2:1)</label>
                                <p style="color:red !important"><small id="error-image"></small></p>
                                @component('common.single_dropzone',
                                    ['id' => "projectImageDropzone",
                                    'object' =>  isset($settings,$settings['project_image']) ? $settings['project_image'] : null, 
                                    'width' => 300, 
                                    'height' => 150,
                                    'imageId' => 'project_image',
                                    'collection_key' => 'header-image'
                                    ])
                                @endcomponent
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label class=" mb-0">Page News (Aspect Ratio 2:1)</label>
                                <p style="color:red !important"><small id="error-image"></small></p>
                                @component('common.single_dropzone',
                                    ['id' => "newsImageDropzone",
                                    'object' =>  isset($settings,$settings['news_image']) ? $settings['news_image'] : null, 
                                    'width' => 300, 
                                    'height' => 150,
                                    'imageId' => 'news_image',
                                    'collection_key' => 'header-image'
                                    ])
                                @endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label class=" mb-0">Page Career (Aspect Ratio 2:1)</label>
                                <p style="color:red !important"><small id="error-image"></small></p>
                                @component('common.single_dropzone',
                                    ['id' => "careerImageDropzone",
                                    'object' =>  isset($settings,$settings['career_image']) ? $settings['career_image'] : null, 
                                    'width' => 300, 
                                    'height' => 150,
                                    'imageId' => 'career_image',
                                    'collection_key' => 'header-image'
                                    ])
                                @endcomponent
                            </fieldset>
                        </div>
                    </div>
                </div>
                @can('page-image-modification')
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-primary">Save changes</button>
                </div>
                @endcan
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
