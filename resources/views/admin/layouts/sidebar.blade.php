<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            @canany(['view-project','project-modification','add-new-project','view-project-category','project-category-modification','add-new-project-category'])
            <li class="nav-item has-sub"><a href="#"><i class="ft-briefcase"></i><span class="menu-title" data-i18n="">Projects</span></a>
                <ul class="menu-content" style="">
                    @canany(['view-project','project-modification','add-new-project'])
                    <li class=""><a class="menu-item" href="{{ route('projects.index') }}">Projects</a>
                    </li>
                    @endcanany

                    @canany(['view-roproject-categoryle','project-category-modification','add-new-project-category'])
                    <li class=""><a class="menu-item" href="{{ route('projects.categories.index') }}">Categories</a>
                    </li>
                    @endcanany
                </ul>
            </li>
            @endcanany

            @canany(['view-news','news-modification','add-new-news'])
            <li class=" nav-item"><a href="{{ route('news.index') }}"><i class="ft-clipboard"></i><span class="menu-title" data-i18n="">News</span></a>
            </li>
            @endcanany

            @canany(['view-career','career-modification','add-new-career'])
                <li class=" nav-item"><a href="{{ route('careers.index') }}"><i class="ft-info"></i><span class="menu-title" data-i18n="">Careers</span></a>
                </li>
            @endcanany

            @canany(['banner-slider','our-story','video'])
            <li class=" nav-item"><a href="{{ route('homePage.index') }}"><i class="ft-home"></i><span class="menu-title" data-i18n="">Home Page</span></a>
            </li>
            @endcanany

            @canany(['management-team','company-profile','chairman-message'])
            <li class=" nav-item"><a href="{{ route('whoAreWe.index') }}"><i class="ft-help-circle"></i><span class="menu-title" data-i18n="">Who Are We</span></a>
            </li>
            @endcanany

            @canany(['contact-us-page'])
            <li class=" nav-item"><a href="{{ route('contactUs.index') }}"><i class="ft-phone"></i><span class="menu-title" data-i18n="">Contact Us</span></a>
            </li>
            @endcanany

            @canany(['about-us-page'])
            <li class=" nav-item"><a href="{{ route('aboutUs.index') }}"><i class="ft-info"></i><span class="menu-title" data-i18n="">About Us</span></a>
            </li>
            @endcanany

            @canany(['view-=page-image','page-image-modification'])
            <li class=" nav-item"><a href="{{ route('settings.page-image.index') }}"><i class="ft-info"></i><span class="menu-title" data-i18n="">Page Image</span></a>
            </li>
            @endcanany

            @canany(['view-social-media','social-media-modification'])
            <li class=" nav-item"><a href="{{ route('settings.social-media.index') }}"><i class="ft-info"></i><span class="menu-title" data-i18n="">Social Media</span></a>
            </li>
            @endcanany

            @canany(['view-seo','seo-modification'])
            <li class=" nav-item"><a href="{{ route('settings.seo.index') }}"><i class="ft-info"></i><span class="menu-title" data-i18n="">SEO</span></a>
            </li>
            @endcanany

            @canany(['view-user','user-modification','add-new-user','view-role','role-modification','add-new-role'])
            <li class="nav-item has-sub"><a href="#"><i class="ft-briefcase"></i><span class="menu-title" data-i18n="">Administrators</span></a>
                <ul class="menu-content" style="">
                    @canany(['view-user','user-modification','add-new-user'])
                    <li class=""><a class="menu-item" href="{{ route('users.index') }}">Users</a>
                    </li>
                    @endcanany

                    @canany(['view-role','role-modification','add-new-role'])
                    <li class=""><a class="menu-item" href="{{ route('roles.index') }}">Roles</a>
                    </li>
                    @endcanany
                </ul>
            </li>
            @endcanany
        </ul>
    </div>
</div>