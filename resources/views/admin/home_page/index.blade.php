@extends('admin.layouts.master')
@section('dropzone-css')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/file-uploaders/dropzone.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/ui/prism.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/file-uploaders/dropzone.css">
@endsection
@section('content')

@canany(['banner-slider'])
{!! Form::open(['route' => 'homePage.saveSlider','method' => 'Post']) !!}
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="heading-buttons1">Image Slides</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <button type="submit" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-save white"></i>
                    </button>
                </div>
            </div>
            <div class="card-content">
                <section class="row">
                    <div class="col-md-12">
                        <fieldset class="form-group" style="padding: 20px">
                            @component('common.success_alert',['key' => 'bigbiz_success'])
                            @endcomponent
                        
                            <div id="id_dropzone" 
                                class="dropzone" 
                                
                                method="post">

                            </form>
                            @component('common.error_helper_text',['key' => "bigbiz_profile"]) 
                            @endcomponent
                            
                        </fieldset>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endcanany

@canany(['our-story'])
{!! Form::open([ 'route' => 'homePage.discoverStory','method' => 'post']) !!}
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="heading-buttons1">Discover our story</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <button type="submit" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-save white"></i>
                    </button>
                </div>
            </div>
            <div class="card-content">
                @component('common.success_alert',['key' => 'discover_us_success'])
                @endcomponent
                <section class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group" style="padding: 20px">
                            <label>Description</label>
                            {!! Form::textarea('description',$home_page->discover_story,['class' => 'form-control']) !!}
                            @component('common.error_helper_text',['key' => "description"]) 
                            @endcomponent
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group" style="padding: 20px">
                            <label>Image</label>
                            @component('common.error_helper_text',['key' => "bdiscover_us_success"]) 
                            @endcomponent
                            @component(
                                    'common.single_dropzone',
                                    [
                                        'id' => "potato",
                                        'object' => isset($home_page) ? $home_page : null, 
                                        'width' => 200, 
                                        'height' => 150,
                                        'collection_key' => 'background',
                                        'imageId' => 'background'
                                    ])
                                @endcomponent
                        </fieldset>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endcanany

{!! Form::open([ 'route' => 'homePage.saveVideoKey','method' => 'post']) !!}
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="heading-buttons1">Video</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <button type="submit" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-save white"></i>
                    </button>
                </div>
            </div>
            <div class="card-content">
                @component('common.success_alert',['key' => 'video_key'])
                @endcomponent
                <section class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group" style="padding: 20px">
                            <label>Video key (eg. R0PrgE_PKSg,7TkIQV3W9kk)</label>
                            <label class="text-danger">***seperate your youtube key with comma (,)</label>
                            {!! Form::textarea('video_key',$home_page->video_key,['class' => 'form-control', 'placeholder' => 'Please enter only youtube key. Eg. R0PrgE_PKSg,7TkIQV3W9kk','rows' => '3']) !!}
                            @component('common.error_helper_text',['key' => "video_key"])@endcomponent
                        </fieldset>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection

@section('page-script')
<script src="/app-assets/vendors/js/extensions/dropzone.min.js"></script>
<script src="/app-assets/vendors/js/ui/prism.min.js"></script>
<script>

    $("#id_dropzone").dropzone({
        url: '/admin/upload-temp-multiple',
        acceptedFiles: "image/jpeg, image/png, image/jpg, image/gif",
        uploadMultiple: true,
        maxFiles: 2000,
        addRemoveLinks: true,
        dictRemoveFileConfirmation: "Are you sure you want to remove this File?",
        init: function(){
            var thisDropzone = this;
            
            $.ajax({
                type: 'get',
                url: '/admin/home-page/get-slider',
                dataType: 'json',
                success: function(response){
                    for (i = 0; i < response.length; i++) {
                        var mockFile = { name: response[i], size: 12345 };

                        // Call the default addedfile event handler
                        thisDropzone.emit("addedfile", mockFile);

                        // And optionally show the thumbnail of the file:
                        thisDropzone.emit("thumbnail", mockFile,response[i]);        

                        thisDropzone.emit("complete", mockFile);
                        thisDropzone.files.push(mockFile); 
                        $('<input>').attr({
                            type: 'hidden',
                            id: response[i],
                            value: response[i],
                            name: "images[]"
                        }).appendTo('#id_dropzone');
                        // $(file.previewTemplate).append('<span class="server_file">'+response[i]+'</span>');    
                    }

                    $('.dz-image img').attr('width','100%')
                    $('.dz-image img').attr('height','100%')
                }
            })

            this.on('sending', function(file, xhr, formData){
                formData.append("_token", $("input[name='_token']").val());
                formData.append("_method", "POST");
            })
            this.on("successmultiple", function(file, response){
                response.forEach(function(value){
                    $('<input>').attr({
                        type: 'hidden',
                        id: value.id,
                        value: value.url,
                        name: "images[]"
                    }).appendTo('#id_dropzone');
                    $(file.previewTemplate).append('<span class="server_file">'+value.url+'</span>');
                })
            }),
            this.on("addedfile", function(event) {
                while (thisDropzone.files.length > thisDropzone.options.maxFiles) {
                    thisDropzone.removeFile(thisDropzone.files[0]);
                }
                $("#id_dropzone").css('height','auto');
            });
            this.on("removedfile", function(file){
                var server_file = file.name
                console.log(thisDropzone.files.length)
                if(thisDropzone.files.length > 0){
                    $('.dz-message').css('display','none')
                }else{
                    $('.dz-message').css('display','block')
                }
                $('#id_dropzone').find('input[id="'+server_file+'"]').remove()
                $.post("/admin/home-page/remove-file", { server_file } );
            })
        }
    });

</script>
@endsection