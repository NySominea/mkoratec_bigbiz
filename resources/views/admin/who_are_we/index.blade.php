@extends('admin.layouts.master')

@section('content')
    @canany(['banner-slider'])
    @component('admin.who_are_we.management_team',['managers' => $managers]) @endcomponent
    <br><br>
    @endcanany

    @canany(['company-profile'])
    @component('admin.who_are_we.profile',['who_we_are' => $who_we_are]) @endcomponent
    <br><br>
    @endcanany

    @canany(['chairman-message'])
    @component('admin.who_are_we.message_from_ceo',['who_we_are' => $who_we_are]) @endcomponent
    @endcanany

@endsection

@section('page-script')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<script>
$(document).ready(function(){
    $('#summernote').summernote({
        dialogsInBody: true,
            callbacks: {
                onInit:function(){
                    $('body > .note-popover').hide();
                }
            }
        });

    $('#btn-add').click(function(){
        $.ajax({
            type : 'POST',
            url : '/admin/who-are-we/save-management-team',
            data : $('#form-add').serialize(),
            dataType: 'json',
            success: function(response){
                if(response.errors){
                    Object.keys(response.errors).forEach(function(key) {
                        $('#error-'+key).html(response.errors[key][0])
                    });
                }else{
                    location.reload()
                }
            }
        })
    })

    $('.btn-update').click(function(){
        var button = $(this)
        var id = button.attr('data-id')

        $.ajax({
            type: 'GET',
            url: '/admin/who-are-we/get-management-team?mid='+id,
            data: $('#form-update').serialize(),
            dataType: 'json',
            success: function(response){
                $('#form-update').find('input[name=update-name]').val(response.name)
                $('#form-update').find('input[name=update-position]').val(response.position)
                $('#form-update').find('input[name=id]').val(response.id)
                $(".dz-preview").remove();
                var mockFile = { name: "Filename", size: 12345 };

                // Call the default addedfile event handler
                myDropZone.emit("addedfile", mockFile);
                // And optionally show the thumbnail of the file:
                myDropZone.emit("thumbnail", mockFile, response.url);
                myDropZone.emit("complete", mockFile);
                myDropZone.files.push(mockFile);

                $('#form-update').find('input[name=secret-id]').val(id)

                $('#modal-update').modal('show');
            }
        })
    })

        $('#update-btn').click(function(){
            $.ajax({
                type: 'POST',
                url: '/admin/who-are-we/update-management-team/'+$('#form-update').find('input[name=secret-id]').val(),
                data: $('#form-update').serialize(),
                dataType: 'json',
                success: function(response){
                    if(response.errors){
                        Object.keys(response.errors).forEach(function(key) {
                            $('#error-'+key).html(response.errors[key][0])
                        });
                    }else{
                        location.reload()
                    }
                }
            })
        })
        $('.btn-confirm').click(function(){
            var button = $(this);
            var id = button.attr('data-id')
            swal({
                title: "Are you sure?",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No, cancel!",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Yes, delete it!",
                        value: true,
                        visible: true,
                        className: "btn-danger",
                        closeModal: true
                    }
                }
            }).then(isConfirm => {
                if (isConfirm) {
                    $.ajax({
                        url: '/admin/who-are-we/remove-management-team/'+id,
                        method: 'delete',
                        dataType: 'json',
                        success: function(response){
                            window.location.reload()
                        }
                    })
                }
            });
        });

        if(window.location.hash) {
            $('html, body').animate({
                scrollTop: $(window.location.hash).offset().top - 70
            }, 2000);
        }
})  
</script>
@endsection