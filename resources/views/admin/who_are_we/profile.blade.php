<div id="profile"></div>

{!! Form::model($who_we_are,['route' => 'whoAreWe.updateProfile','method' => 'patch']) !!}
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="heading-buttons1">BIGBIZ Profile</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <button type="submit" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-save white"></i>
                    </button>
                </div>
            </div>
            <div class="card-content">
                <section class="row">
                    <div class="col-md-12">
                        <fieldset class="form-group" style="padding: 20px">
                            @component('common.success_alert',['key' => 'bigbiz_success'])
                            @endcomponent
                            @component('common.error_helper_text',['key' => "bigbiz_profile"]) 
                            @endcomponent
                            <textarea id="summernote" name="bigbiz_profile">{{ $who_we_are->bigbiz_profile }}</textarea>
                        </fieldset>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
