<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="heading-buttons1">BIGBIZ Management Team</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <button type="button" class="btn btn-sm btn-icon btn-success" data-toggle="modal" data-target="#default">
                        <i class="ft-plus white"></i>
                    </button>
                </div>
            </div>
            <div class="card-content">
                <section id="simple-user-cards" class="row">
                    @foreach($managers as $manager)
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="card">
                            <div class="text-center">
                                <div class="card-body">
                                    <img src="{{ $manager->getMedia('image')->first() ? $manager->getMedia('image')->first()->getUrl() : '' }}" class="rounded-circle  height-150" alt="Card image">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title">{{ $manager->name }}</h4>
                                    <h6 class="card-subtitle text-muted">{{ $manager->position }}</h6>
                                </div>
                                <div class="text-center">
                                    <a href="javascript:void(0)" class="btn btn-social-icon mr-1 mb-1 btn-outline-primary btn-update" data-id="{{ $manager->id }}"><span class="fa fa-pencil"></span></a>
                                    <a href="javascript:void(0)" class="btn btn-social-icon mr-1 mb-1 btn-outline-danger btn-confirm" data-id="{{ $manager->id }}"><span class="fa fa-trash"></span></a>                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </section>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Add Management Member</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-add">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label for="basicInput">Name <span class="required">*</span></label>
                            {!! Form::text('name',null,['class' => 'form-control']) !!}
                            <p style="color:red !important"><small id="error-name"></small></p>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="basicInput">Position <span class="required">*</span></label>
                            {!! Form::text('position',null,['class' => 'form-control']) !!}
                            <p style="color:red !important"><small id="error-position"></small></p>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <label>Thumbnail Image (Aspec Ratio 1:1)</label>
                            <p style="color:red !important"><small id="error-image"></small></p>
                            @component('common.single_dropzone',
                                ['id' => "singleDropzone",
                                'object' =>  null, 
                                'width' => 200, 
                                'height' => 150,
                                'imageId' => 'image',
                                'collection_key' => 'image'
                                ])
                            @endcomponent
                        </fieldset>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button id="btn-add" type="button" class="btn btn-outline-primary">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">Update Management Member</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-update">
                        <input type="hidden" name="id"/>
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="basicInput">Name <span class="required">*</span></label>
                                {!! Form::text('update-name',null,['class' => 'form-control']) !!}
                                <p style="color:red !important"><small id="error-update-name"></small></p>
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Position <span class="required">*</span></label>
                                {!! Form::text('update-position',null,['class' => 'form-control']) !!}
                                <p style="color:red !important"><small id="error-update-position"></small></p>
                            </fieldset>
                            <input type="hidden" name="secret-id"/>
                        </div>
                        <div class="col-md-6">
                            <fieldset>
                                <label>Thumbnail Image (Aspec Ratio 1:1)</label>
                                <p style="color:red !important"><small id="error-update-image"></small></p>
                                @component('common.single_dropzone',
                                    ['id' => "potato",
                                    'object' =>  null, 
                                    'width' => 200, 
                                    'height' => 150,
                                    'imageId' => 'background',
                                    'collection_key' => 'image'
                                    ])
                                @endcomponent
                            </fieldset>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button id="update-btn" type="button" class="btn btn-outline-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>