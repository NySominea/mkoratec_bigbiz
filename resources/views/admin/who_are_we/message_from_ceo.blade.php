{!! Form::model($who_we_are,['route' => 'whoAreWe.updateMessage','method' => 'patch']) !!}
<div class="row" id="message">
    <div class="col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="heading-buttons1">Message From CEO/Chairman</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <button type="submit" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-save white"></i>
                    </button>
                </div>
            </div>
            <div class="card-content" style="padding: 20px">
                    @component('common.success_alert',['key' => 'message_success'])
                    @endcomponent
                <section class="row">
                    <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="form-group" style="padding: 20px">
                                        <label for="basicInput">Mr./Mrs.</label>
                                        {!! Form::text('chairman_name',null,['class' => 'form-control']) !!}
                                        @component('common.error_helper_text',['key' => "title"]) 
                                        @endcomponent
                                    </fieldset>
                                    <fieldset class="form-group" style="padding: 20px">
                                        <label for="basicInput">Messages:</label>
                                        {!! Form::textarea('message',null,['class' => 'form-control']) !!}
                                        @component('common.error_helper_text',['key' => "title"]) 
                                        @endcomponent
                                    </fieldset>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                            <label for="basicInput">CEO Image:</label>
                            @component('common.single_dropzone',
                            ['id' => "ceo",
                            'object' =>  isset($who_we_are) ? $who_we_are : null, 
                            'width' => 200, 
                            'height' => 150,
                            'imageId' => 'ceo-image',
                            'collection_key' => 'ceo-image'
                            ])
                            @endcomponent
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
