@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="card" >
            <div class="card-header">
                <h4 class="card-title" id="heading-buttons1">Contact Us</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                </div>
            </div>
            <div class="card-content" style="padding: 20px">
                @component('common.success_alert',['key' => 'success'])
                @endcomponent
                {!! Form::model($contact_us,['route' => 'contactUs.update','method' => 'patch']) !!}
                <section class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <label for="basicInput">Email: </label>
                            {!! Form::text('email',null,['class' => 'form-control']) !!}
                            @component('common.error_helper_text',['key' => "email"]) 
                            @endcomponent
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="basicInput">Tel: </label>
                            {!! Form::text('tel',null,['class' => 'form-control']) !!}
                            @component('common.error_helper_text',['key' => "tel"]) 
                            @endcomponent
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="basicInput">Fax:</label>
                            {!! Form::text('fax',null,['class' => 'form-control']) !!}
                            @component('common.error_helper_text',['key' => "fax"]) 
                            @endcomponent
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="basicInput">Address</label>
                            {!! Form::textarea('address',null,['class' => 'form-control','rows' => 3]) !!}
                            @component('common.error_helper_text',['key' => "address"]) 
                            @endcomponent
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="basicInput">Map Address</label>
                            <label class="text-danger d-block">***Enter only map src</label>
                            {!! Form::textarea('map',null,['class' => 'form-control','rows' => 3]) !!}
                            @component('common.error_helper_text',['key' => "map"]) 
                            @endcomponent
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <label>Background Image (Aspect Ratio 1:1)</label>
                            <p style="color:red !important"><small id="error-image"></small></p>
                            @component('common.single_dropzone',
                                ['id' => "singleDropzone",
                                'object' =>  isset($contact_us) ? $contact_us : null, 
                                'width' => 300, 
                                'height' => 300,
                                'imageId' => 'image',
                                'collection_key' => 'images'
                                ])
                            @endcomponent
                        </fieldset>
                    </div>
                </section>
                <section class="row">
                    <div class="col-md-12">
                        <button class="btn btn-success float-right">Save Changed</button>
                        <button class="btn btn-danger float-right mr-1 mb-1">Discard</button>
                    </div>
                </section>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection