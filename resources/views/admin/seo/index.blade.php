@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">SEO</h4>
            </div>

            {{ Form::model($settings,['route' => ['settings.seo.update'], 'method' => 'POST']) }}
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label for="basicInput">SEO Keywords</label>
                                {!! Form::textarea('seo_keyword',isset($settings,$settings['seo_keyword']) ? $settings['seo_keyword']->value : '',['class' => 'form-control', 'placeholder' => 'Enter SEO keywords','rows'=>'2']) !!}
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">SEO Description</label>
                                {!! Form::textarea('seo_description',isset($settings,$settings['seo_description']) ? $settings['seo_description']->value : '',['class' => 'form-control', 'placeholder' => 'Enter SEO description','rows'=>'5']) !!}
                            </fieldset>
                        </div>
                    </div>
                </div>
                @can('seo-modification')
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-primary">Save changes</button>
                </div>
                @endcan
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
