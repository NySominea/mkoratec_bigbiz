@php $key = isset($key) ? $key : 'success' @endphp
@if(session()->has($key))
<div class="alert alert-success mb-2" role="alert">
    <strong>{{ session()->get($key)}}</strong> 
</div>
@endif