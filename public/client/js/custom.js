
// NAVBAR MENU
$(document).ready(function(){
    let windowScroll = $(window).scrollTop()
    let nav = $("#navbar");

    // navbar trasnformation on scroll
    changeNavStyleOnScroll(windowScroll);
    $(window).scroll(function() {
      windowScroll = $(this).scrollTop();
      changeNavStyleOnScroll(windowScroll);
      
    });

    function changeNavStyleOnScroll(windowScroll){
            // $('.navbar-collapse').collapse('hide');
        if(windowScroll >= 1) {
            nav.addClass('sticky bg-white sticky-top')
        }else{
            windowScroll = 0;
            nav.removeClass('sticky bg-white sticky-top')
        }
    }

    $(".nav-link.active").parents('.nav-item').addClass('active');

    $('.nav-item.dropdown').on('click', function() {
        location.href = $(this).find('.nav-link').attr('href');
    });
})

// Check Navbar Active
$(document).ready(function(){
    var current = (location.origin).concat(location.pathname).concat(location.hash).replace(/\/$/, '');
    
    $('.nav-item a').each(function(){
        if ($(this).attr('href').replace(/\/$/, '') == current) {
            $(this).addClass('active');
            $(this).parents('.nav-item').addClass('active');
        }
    })
})


// END NAVBAR MENU

// OWL CAROUSEL
// $('#homepage .our-team .owl-carousel').owlCarousel({
//     loop:true,
//     margin:40,
//     nav:true,
//     autoplay:true,
//     autoplayTimeout:5000,
//     autoHeight:true,
//     navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
//     responsive:{
//         0:{
//             items:1,
//             nav:false,
//         },
//         600:{
//             items:1,
//             nav:true,
//             dots:true,
//         },
//         768:{
//             items:1,
//             nav:true,
//             dots:true,
//         },
//         1200:{
//             items:1,
//             nav:true,
//             dots:true,
//         }
//     }
// });

//====Scroll UP button ====
$(document).ready(function(){
    btn = $("#btnScrollUp");

    btn.click(function(){
        $('html,body').animate({
            'scrollTop' : 0,
        },500)
    });
    $(window).on('scroll',function(){
        if($(this).scrollTop() > 100){
            if(!btn.is(':visible')){
                btn.fadeIn('slow');
            }
        }else{
            btn.fadeOut();
        }
    })
});

// RESIZE HEIGHT SLIDER 
// resizeSlider();
// $(window).resize(function(){
//     resizeSlider();
// });


function resizeSlider(){
    var height = $(window).outerHeight();
    var width = $(window).outerWidth();
    var selector = $('#homepage .carousel-inner .carousel-item');
    if(width > 468){
        height = height;
    }
    // else if(width > 576){
    //     height = (height * 5) / 6;
    // }else if(width > 468){ 
    //     height = (height * 5) / 7;
    // }
    else{
        height = height / 1.5;
    } 
    selector.css('height',height + 'px');
}


