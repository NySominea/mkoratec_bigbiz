
var dropzones = [
    { 'dropzoneSelector' : $('#profileDropzone'), 'inputSelector' : $('#image'), 'width' : 150, 'height' : 150},
    { 'dropzoneSelector' : $('#projectDropzone'), 'inputSelector' : $('#image'), 'width' : 300, 'height' : 200},
    { 'dropzoneSelector' : $('#potato'), 'inputSelector' : $('#background'), 'width' : 120, 'height' : 120},
    { 'dropzoneSelector' : $('#singleDropzone'), 'inputSelector' : $('#image'), 'width' : 120, 'height' : 120},
    { 'dropzoneSelector' : $('#ceo'), 'inputSelector' : $('#ceo-image'), 'width' : 120, 'height' : 120},
    { 'dropzoneSelector' : $('#aboutImageDropzone'), 'inputSelector' : $('#about_us_image'), 'width' : 300, 'height' : 150},
    { 'dropzoneSelector' : $('#contactImageDropzone'), 'inputSelector' : $('#contact_us_image'), 'width' : 300, 'height' : 150},
    { 'dropzoneSelector' : $('#projectImageDropzone'), 'inputSelector' : $('#project_image'), 'width' : 300, 'height' : 150},
    { 'dropzoneSelector' : $('#newsImageDropzone'), 'inputSelector' : $('#news_image'), 'width' : 300, 'height' : 150},
    { 'dropzoneSelector' : $('#careerImageDropzone'), 'inputSelector' : $('#career_image'), 'width' : 300, 'height' : 150},
];
var myDropZone;
$.each(dropzones, function( index, value ) {
    if(value.dropzoneSelector.length > 0){ 
        var myDropzone = value.dropzoneSelector;
        var inputSelector = value.inputSelector;
        var width = value.width;
        var height = value.height;
        
        myDropzone.dropzone({
            acceptedFiles: "image/jpeg, image/png, image/jpg, image/gif",
            maxFiles: 1,
            uploadMultiple: false,
            addRemoveLinks: true,
            dictRemoveFileConfirmation: "Are you sure you want to remove this File?",
            thumbnailWidth: width,
            thumbnailHeight: height,
        
            init: function(){
                if(index == 2) myDropZone = this;
                var thisDropzone = this;
                // alert(inputSelector.id)
                if($('#action'+inputSelector.attr('id')).val() == 'update'){ console.log(inputSelector.data('url'))
                    var name = inputSelector.data('name');
                    var size = inputSelector.data('size');
                    var url  = inputSelector.data('url');
                    if(url){
                        myDropzone.css('height','auto');
                        var mockFile = { name: name, size: size, accepted: true };  
                        
                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, url);
                        thisDropzone.files.push(mockFile);
                        thisDropzone.emit("complete", mockFile);
                        thisDropzone.options.maxFiles = thisDropzone.options.maxFiles;
                        thisDropzone._updateMaxFilesReachedClass();
                    } 
                }
                this.on('sending', function(file, xhr, formData){
                    console.log('sending')
                    formData.append("_token", $("input[name='_token']").val());
                    formData.append("_method", "POST");
                })
                this.on("success", function(file, response){
                    inputSelector.val(response.path);
                }),
                this.on("addedfile", function(event) {
                    while (thisDropzone.files.length > thisDropzone.options.maxFiles) {
                        thisDropzone.removeFile(thisDropzone.files[0]);
                    }
                    myDropzone.css('height','auto');
                });
                this.on("removedfile", function(file){
                    file.previewElement.remove();
                    if(this.files.length < 1){
                        myDropzone.css('height','');
                    }
                })
            } 
        })
    }
});



